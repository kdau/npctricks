# ![[icon]](promo/icon.png) NPC Tricks

*a [Stardew Valley](http://stardewvalley.net/) mod by [kdau](https://www.kdau.com)*

Sit. Stay. Good NPC! This framework lets Stardew Valley NPCs have custom sprite rendering, movement styles, reactions and scheduled behaviors. Use it to make NPC humans with special attributes or behaviors; NPC pets, wild animals or monsters; or anything beyond.

## ![Compatibility](https://www.kdau.com/headers/compatibility.png)

**Game:** Stardew Valley 1.5+

**Platform:** Linux, macOS or Windows

**Multiplayer:** works; every player must install

**Other mods:** This framework only affects NPCs that have been coded to use it.

## ![Installation](https://www.kdau.com/headers/installation.png)

1. Install [SMAPI](https://smapi.io/)
1. Install [Expanded Preconditions Utility](https://www.nexusmods.com/stardewvalley/mods/6529)
1. Download this mod from [Nexus](https://www.nexusmods.com/stardewvalley/mods/TODO?tab=files) or [ModDrop](https://www.moddrop.com/stardew-valley/mods/TODO)
1. Unzip and put the `NPCTricks` folder inside your `Mods` folder
1. Run the game using SMAPI

## ![Use](https://www.kdau.com/headers/use.png)

TODO: ...

## ![Translation](https://www.kdau.com/headers/translation.png)

No translations are available yet.

This mod can be translated into any language supported by the game. Your contribution would be welcome. Please see the [instructions on the wiki](https://stardewvalleywiki.com/Modding:Translations). You can send me your work in [a GitLab issue](https://gitlab.com/kdau/npctricks/-/issues) or [a Nexus message](https://www.nexusmods.com/stardewvalley/mods/TODO?tab=posts).

## ![Acknowledgments](https://www.kdau.com/headers/acknowledgments.png)

* Like all mods, this one is indebted to ConcernedApe, Pathoschild and the various framework modders.
* This mod is originally based on ideas from [EssGee](https://www.nexusmods.com/stardewvalley/users/83595503) and [LemurKat](https://www.nexusmods.com/users/68088657).
* The #making-mods channel on the [Stardew Valley Discord](https://discordapp.com/invite/StardewValley) offered valuable guidance and feedback.

## ![See also](https://www.kdau.com/headers/see-also.png)

* [Release notes](doc/RELEASE-NOTES.md)
* [Source code](https://gitlab.com/kdau/npctricks)
* [Report bugs](https://gitlab.com/kdau/npctricks/-/issues)
* [My other Stardew stuff](https://www.kdau.com/stardew)
* Mirrors:
	[Nexus](https://www.nexusmods.com/stardewvalley/mods/TODO),
	[ModDrop](https://www.moddrop.com/stardew-valley/mods/TODO),
	[forums](https://forums.stardewvalley.net/index.php?resources/TODO/)

Other relevant frameworks for NPC modders:

* [Anti-Social NPCs](https://www.nexusmods.com/stardewvalley/mods/5371)
* [Custom NPC Fixes](https://www.nexusmods.com/stardewvalley/mods/3849)
