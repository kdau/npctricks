[size=6][b][img]https://www.kdau.com/NPCTricks/icon.png[/img] NPC Tricks[/b][/size]

[i]a [url=http://stardewvalley.net/]Stardew Valley[/url] mod by [url=https://www.kdau.com]kdau[/url][/i]

Sit. Stay. Good NPC! This framework lets Stardew Valley NPCs have custom sprite rendering, movement styles, reactions and scheduled behaviors. Use it to make NPC humans with special attributes or behaviors; NPC pets, wild animals or monsters; or anything beyond.

[size=4][b][img alt="Compatibility"]https://www.kdau.com/headers/compatibility.png[/img][/b][/size]

[b]Game:[/b] Stardew Valley 1.5+

[b]Platform:[/b] Linux, macOS or Windows

[b]Multiplayer:[/b] works; every player must install

[b]Other mods:[/b] This framework only affects NPCs that have been coded to use it.

[size=4][b][img alt="Installation"]https://www.kdau.com/headers/installation.png[/img][/b][/size]

[list=1]
[*]Install [url=https://smapi.io/]SMAPI[/url]
[*]Install [url=https://www.nexusmods.com/stardewvalley/mods/6529]Expanded Preconditions Utility[/url]
[*]Download this mod from the link in the header above
[*]Unzip and put the [icode]NPCTricks[/icode] folder inside your [icode]Mods[/icode] folder
[*]Run the game using SMAPI
[/list]

[size=4][b][img alt="Use"]https://www.kdau.com/headers/use.png[/img][/b][/size]

TODO: ...

[size=4][b][img alt="Translation"]https://www.kdau.com/headers/translation.png[/img][/b][/size]

No translations are available yet.

This mod can be translated into any language supported by the game. Your contribution would be welcome. Please see the [url=https://stardewvalleywiki.com/Modding:Translations]instructions on the wiki[/url]. You can send me your work in [url=https://gitlab.com/kdau/npctricks/-/issues]a GitLab issue[/url] or a message here.

[size=4][b][img alt="Acknowledgments"]https://www.kdau.com/headers/acknowledgments.png[/img][/b][/size]

[list]
[*]Like all mods, this one is indebted to ConcernedApe, Pathoschild and the various framework modders.
[*]This mod is originally based on ideas from [url=https://www.nexusmods.com/stardewvalley/users/83595503]EssGee[/url] and [url=https://www.nexusmods.com/users/68088657]LemurKat[/url].
[*]The #making-mods channel on the [url=https://discordapp.com/invite/StardewValley]Stardew Valley Discord[/url] offered valuable guidance and feedback.
[/list]

[size=4][b][img alt="See also"]https://www.kdau.com/headers/see-also.png[/img][/b][/size]

[list]
[*][url=https://gitlab.com/kdau/npctricks/-/blob/main/doc/RELEASE-NOTES.md]Release notes[/url]
[*][url=https://gitlab.com/kdau/npctricks]Source code[/url]
[*][url=https://gitlab.com/kdau/npctricks/-/issues]Report bugs[/url]
[*][url=https://www.kdau.com/stardew]My other Stardew stuff[/url]
[*]Mirrors: [url=https://www.nexusmods.com/stardewvalley/mods/TODO]Nexus[/url], [url=https://www.moddrop.com/stardew-valley/mods/TODO]ModDrop[/url], [b]forums[/b]
[/list]
Other relevant frameworks for NPC modders:

[list]
[*][url=https://www.nexusmods.com/stardewvalley/mods/5371]Anti-Social NPCs[/url]
[*][url=https://www.nexusmods.com/stardewvalley/mods/3849]Custom NPC Fixes[/url]
[/list]
