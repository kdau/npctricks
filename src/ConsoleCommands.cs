using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using StardewModdingAPI;
using StardewValley;
using System;
using System.Collections.Generic;

namespace NPCTricks
{
	internal class RectangleConverter : JsonConverter
	{
		public override bool CanRead => false;
		public override bool CanWrite => true;

		public override bool CanConvert (Type objectType)
		{
			return objectType == typeof (Rectangle);
		}

		public override object ReadJson (JsonReader reader, Type objectType,
			object existingValue, JsonSerializer serializer)
		{
			throw new InvalidOperationException ("can't read");
		}

		public override void WriteJson (JsonWriter writer, object value,
			JsonSerializer serializer)
		{
			Rectangle r = (Rectangle) value;
			writer.WriteRawValue ($"({r.X},{r.Y},{r.Width},{r.Height})");
		}
	}

	public partial class ModEntry
	{
		private void addConsoleCommands ()
		{
			Helper.ConsoleCommands.Add ("list_tricks",
				"Lists the tricks currently assigned to NPC(s).\n\nUsage: list_tricks [S:name]\n- name: name of NPC to list tricks for; if not given, list tricks for every NPC",
				cmdListTricks);
			Helper.ConsoleCommands.Add ("rebuild_tricks",
				"Rebuilds the tricks assigned to NPCs from the content packs.\n\nUsage: rebuild_tricks [B:reload]\n- reload: whether to reload the content packs from their files (default false)",
				cmdRebuildTricks);
		}

		private void cmdListTricks (string _command, string[] args)
		{
			try
			{
				if (!Context.IsWorldReady)
					throw new Exception ("Cannot list tricks without an active save.");

				IEnumerable<Trickster> tricksters;
				if (args.Length > 0)
				{
					var nameBank = new List<string> (this.tricksters.Keys);
					var matchingName = Utility.fuzzySearch (args[0], nameBank);
					Trickster trickster = null;
					if (matchingName != null)
						trickster = this.tricksters[matchingName];
					tricksters = new Trickster[] { trickster };
				}
				else
				{
					tricksters = this.tricksters.Values;
				}

				JsonSerializerSettings settings = new ();
				settings.Formatting = Formatting.None;
				settings.Converters.Add (new StringEnumConverter ());
				settings.Converters.Add (new RectangleConverter ());

				foreach (Trickster trickster in tricksters)
				{
					Monitor.Log ($"{trickster.name}:", LogLevel.Alert);
					foreach (Trick trick in trickster.tricks ())
					{
						Monitor.Log ($"    {trick.GetType ().Name}:",
							LogLevel.Info);
						Monitor.Log ("        " + JsonConvert.SerializeObject (trick,
							settings), LogLevel.Debug);
					}
				}
			}
			catch (Exception e)
			{
				Utilities.Log ("list_tricks", e.Message, LogLevel.Error);
				Utilities.Log (null, e.StackTrace, LogLevel.Trace);
			}
		}

		private void cmdRebuildTricks (string _command, string[] args)
		{
			try
			{
				if (!Context.IsWorldReady)
					throw new Exception ("Cannot rebuild tricks without an active save.");

				if (args.Length > 0 && Boolean.Parse (args[0]))
					loadContentPacks ();

				rebuildTricks ();
				Monitor.Log ("Rebuilt tricks for NPCs", LogLevel.Info);
			}
			catch (Exception e)
			{
				Utilities.Log ("rebuild_tricks", e.Message, LogLevel.Error);
				Utilities.Log (null, e.StackTrace, LogLevel.Trace);
			}
		}
	}
}
