using ExpandedPreconditionsUtility;
using StardewModdingAPI;
using StardewModdingAPI.Events;
using StardewValley;
using System;

namespace NPCTricks
{
	public partial class ModEntry
	{
		private void addEventHandlers ()
		{
			Helper.Events.GameLoop.GameLaunched += onGameLaunched;
			Helper.Events.GameLoop.SaveCreated += onSaveCreatedOrLoaded;
			Helper.Events.GameLoop.SaveLoaded += onSaveCreatedOrLoaded;
			Helper.Events.GameLoop.DayStarted += onDayStarted;
			Helper.Events.GameLoop.DayEnding += onDayEnding;
			Helper.Events.GameLoop.UpdateTicked += onUpdateTicked;
			Helper.Events.Input.ButtonPressed += onButtonPressed;
			Helper.Events.Display.RenderedWorld += onRenderedWorld;
		}

		private void onGameLaunched (object _sender, GameLaunchedEventArgs _e)
		{
			// Make Json Assets available for custom object ID lookup.
			jsonAssets = Helper.ModRegistry.GetApi<JsonAssets.IApi>
				("spacechase0.JsonAssets");

			// Make Expanded Preconditions Utility available for checking.
			conditionsChecker = Helper.ModRegistry.GetApi<IConditionsChecker> ("Cherry.ExpandedPreconditionsUtility");
			conditionsChecker.Initialize (Config.VerboseLogging, ModManifest.UniqueID);

			// Hook into Save Anywhere for compatibility with it.
			var SaveAnywhere = Helper.ModRegistry.GetApi
				<Omegasis.SaveAnywhere.Framework.ISaveAnywhereAPI>
				("Omegasis.SaveAnywhere");
			if (SaveAnywhere != null)
			{
				SaveAnywhere.BeforeSave += onDayEnding;
				SaveAnywhere.AfterSave += onDayStarted;
				SaveAnywhere.AfterLoad += onSaveCreatedOrLoaded;
			}

			// Load this mod's content packs.
			loadContentPacks ();
		}

		private void onSaveCreatedOrLoaded (object _sender, EventArgs _e)
		{
			tricksters.Clear ();
			buildTricks ();
		}

		private void onDayStarted (object _sender, EventArgs _e)
		{
			buildTricks ();
		}

		private void onDayEnding (object _sender, EventArgs _e)
		{
			destroyTricks ();
		}

		private void onUpdateTicked (object _sender, UpdateTickedEventArgs e)
		{
			if (!Context.IsWorldReady)
				return;

			if (e.IsMultipleOf (6))
			{
				// Update for NPCs that have entered or exited synced locations.
				if (!Context.IsMainPlayer)
					bindTricksters ();

				// Update for dynamic conditions on additionals.
				foreach (Trickster trickster in tricksters.Values)
					trickster.updateDynamic ();
			}

			foreach (Trickster trickster in tricksters.Values)
				trickster.update (Game1.currentGameTime);

			if (e.IsOneSecond && !Game1.eventUp)
			{
				foreach (Trickster trickster in tricksters.Values)
				{
					if (trickster.tryGreetings ())
						break;
				}
			}
		}

		private void onButtonPressed (object _sender, ButtonPressedEventArgs e)
		{
			if (!Context.IsWorldReady || !Context.IsPlayerFree)
				return;

			// Check platform-specific interaction constraints.
			if ((Constants.TargetPlatform == GamePlatform.Android)
					? !e.Cursor.GrabTile.Equals (e.Cursor.Tile)
					: !e.Button.IsActionButton ())
				return;

			// Ignore clicks that would lead to other interactions.
			if (Utilities.CheckForCharacterInteractionSafely (e.Cursor.Tile))
				return;

			// Track clicks for showing hidden emotes.
			HiddenEmote.HandleWorldAction (e.Cursor.Tile);
		}

		private void onRenderedWorld (object _sender, RenderedWorldEventArgs e)
		{
			if (!Context.IsWorldReady || !Config.BoundingBoxes)
				return;

			// Draw bounding boxes for tricksters.
			// Adapted from code provided by spacechase0.
			foreach (NPC npc in Game1.currentLocation.characters)
			{
				Trickster trickster = Utilities.GetTrickster (npc);
				if (trickster?.Sprite != null)
					trickster.Sprite.drawBoundingBoxes (e.SpriteBatch);
			}
		}
	}
}
