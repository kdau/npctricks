using Newtonsoft.Json;
using StardewValley;
using System.Collections.Generic;

namespace NPCTricks
{
	public class Additional : BagOfTricks
	{
		[JsonIgnore]
		public ContentFile parent { get; internal set; }

#pragma warning disable IDE1006

		public DynamicConditions DynamicConditions { get; set; } = null;

#pragma warning restore IDE1006

		public bool shouldApplyNow (NPC npc)
		{
			return DynamicConditions == null || DynamicConditions.check (npc);
		}

		public override IEnumerable<string> effectiveNPCs =>
			(NPCs.Count > 0) ? NPCs : parent.effectiveNPCs;

		internal override bool merge (Tricktionary tricksters)
		{
			if (DynamicConditions == null)
				return base.merge (tricksters);

			if (!shouldApply ())
				return false;

			foreach (string npcName in effectiveNPCs)
			{
				if (!tricksters.ContainsKey (npcName))
					tricksters[npcName] = new Trickster (npcName);
				tricksters[npcName].addDynamicAdditional (this);
			}

			return true;
		}
	}
}
