using ExpandedPreconditionsUtility;
using Newtonsoft.Json.Linq;
using StardewModdingAPI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NPCTricks
{
	public class BagOfTricks : TrickContext
	{
		private static IConditionsChecker ConditionsChecker =>
			ModEntry.Instance.conditionsChecker;

#pragma warning disable IDE1006

		public List<string> NPCs { get; set; } = new List<string> ();

		public List<object> Conditions { get; set; } = new List<object> ();

#pragma warning restore IDE1006

		public bool shouldApply ()
		{
			return Conditions.All ((condition) =>
			{
				Type type = condition.GetType ();

				if (type == typeof (string))
					return ConditionsChecker.CheckConditions ((string) condition);

				if (type == typeof (JArray))
				{
					var subconditions = ((JArray) condition)
						.Where ((token) => token.Type == JTokenType.String)
						.Select ((token) => (string) token)
						.ToArray ();
					return ConditionsChecker.CheckConditions (subconditions);
				}

				Utilities.Log ("Conditions", $"Unsupported type {type.Name}");
				return false;
			});
		}

		public virtual IEnumerable<string> effectiveNPCs => NPCs;

		internal virtual bool merge (Tricktionary tricksters)
		{
			if (!shouldApply ())
				return false;

			foreach (string npcName in effectiveNPCs)
			{
				if (!tricksters.ContainsKey (npcName))
					tricksters[npcName] = new Trickster (npcName);
				merge (tricksters[npcName]);
			}

			return true;
		}
	}
}
