using Newtonsoft.Json;
using System.Collections.Generic;

namespace NPCTricks
{
	public class ContentFile : BagOfTricks
	{
		[JsonIgnore]
		public ContentPack parent { get; private set; }

		[JsonIgnore]
		public string filename { get; private set; }

#pragma warning disable IDE1006

		public List<Additional> Additional { get; set; } = new List<Additional> ();

#pragma warning restore IDE1006

		internal static ContentFile Load (ContentPack parent, string filename)
		{
			Utilities.Log (null, $"        {filename}");

			ContentFile contentFile = parent.metadata.ReadJsonFile<ContentFile> (filename);
			contentFile.parent = parent;
			contentFile.filename = filename;

			foreach (Additional additional in contentFile.Additional)
				additional.parent = contentFile;

			return contentFile;
		}

		internal override bool merge (Tricktionary tricksters)
		{
			if (!base.merge (tricksters))
				return false;

			foreach (Additional additional in Additional)
				additional.merge (tricksters);

			return true;
		}
	}
}
