using StardewModdingAPI;
using System;
using System.Collections.Generic;
using System.IO;

namespace NPCTricks
{
	public class ContentPack
	{
		protected static IModHelper Helper => ModEntry.Instance.Helper;
		protected static IMonitor Monitor => ModEntry.Instance.Monitor;
		protected static ModConfig Config => ModConfig.Instance;

		public readonly IContentPack metadata;
		public readonly List<ContentFile> files = new ();

		internal ContentPack (IContentPack metadata)
		{
			this.metadata = metadata;

			Utilities.Log (null,
				$"    {metadata.Manifest.Name} {metadata.Manifest.Version} by {metadata.Manifest.Author}",
				LogLevel.Info, verbose: false);

			foreach (string filePath in
				Directory.GetFiles (metadata.DirectoryPath, "*.json"))
			{
				string filename = Path.GetFileName (filePath);
				if (filename.ToLower () == "manifest.json")
					continue;

				try
				{
					files.Add (ContentFile.Load (this, filename));
				}
				catch (Exception e)
				{
					Utilities.Log (null,
						$"        Skipping {filename} due to load error: {e.Message}",
						LogLevel.Warn);
					Utilities.Log (null, e.StackTrace, LogLevel.Trace);
				}
			}

			if (files.Count == 0)
			{
				Utilities.Log (null,
					$"        No valid content files found in {metadata.Manifest.Name}",
					LogLevel.Alert);
			}
		}

		internal void merge (Tricktionary tricksters)
		{
			foreach (ContentFile file in files)
				file.merge (tricksters);
		}
	}
}
