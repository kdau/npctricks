using Microsoft.Xna.Framework;
using StardewValley;

namespace NPCTricks
{
	public class DynamicConditions
	{
#pragma warning disable IDE1006

		public int Time { get; set; } = -1;
		public int FromTime { get; set; } = -1;
		public int ToTime { get; set; } = -1;
		public bool Day { get; set; } = true;
		public bool Night { get; set; } = true;

		public string Location { get; set; } = null;
		public Rectangle Position { get; set; } = Rectangle.Empty;
		public bool Indoors { get; set; } = true;
		public bool Outdoors { get; set; } = true;

#pragma warning restore IDE1006

		public bool check (NPC npc)
		{
			if (npc == null || npc.currentLocation == null)
				return false;

			int fromTime = (FromTime == -1) ? Time : FromTime;
			if (fromTime != -1 && Game1.timeOfDay < fromTime)
				return false;

			int toTime = (ToTime == -1) ? Time : ToTime;
			if (toTime != -1 && Game1.timeOfDay > toTime)
				return false;

			if (!Day && Game1.timeOfDay < 1800)
				return false;

			if (!Night && Game1.timeOfDay >= 1800)
				return false;

			if (Location != null && npc.currentLocation.Name != Location)
				return false;

			if (Position != Rectangle.Empty &&
					!Position.Contains (npc.getTileLocationPoint ()))
				return false;

			if (!Indoors && !npc.currentLocation.IsOutdoors)
				return false;

			if (!Outdoors && npc.currentLocation.IsOutdoors)
				return false;

			return true;
		}
	}
}
