using StardewValley;

namespace NPCTricks
{
	public abstract class EnvironmentalReaction : Reaction
	{
#pragma warning disable IDE1006

		public double Initiate { get; set; } = 1.0;
		public int Range { get; set; } = 4;

		public bool Indoors { get; set; } = true;
		public bool Outdoors { get; set; } = true;

		public bool Standing { get; set; } = true;
		public bool Moving { get; set; } = true;

		public bool Day { get; set; } = true;
		public bool Night { get; set; } = true;

#pragma warning restore IDE1006

		protected void cloneInherited (EnvironmentalReaction clone)
		{
			base.cloneInherited (clone);

			clone.Initiate = Initiate;
			clone.Range = Range;

			clone.Indoors = Indoors;
			clone.Outdoors = Outdoors;

			clone.Standing = Standing;
			clone.Moving = Moving;

			clone.Day = Day;
			clone.Night = Night;
		}

		// Checks all environmental reaction conditions other than Range.
		protected bool shouldInitiate ()
		{
			return npc != null && npc.currentLocation != null &&
				!Game1.eventUp &&
				Game1.random.NextDouble () < Initiate &&
				(Indoors || npc.currentLocation.IsOutdoors) &&
				(Outdoors || !npc.currentLocation.IsOutdoors) &&
				(Standing || npc.isMoving ()) &&
				(Moving || !npc.isMoving ()) &&
				(Day || Game1.timeOfDay >= 1800) &&
				(Night || Game1.timeOfDay < 1800);
		}
	}
}
