using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NPCTricks
{
	public abstract class Reaction : Trick
	{
#pragma warning disable IDE1006

		public List<int> Animation { get; set; } = new List<int> ();
		public int AnimationDirectionOffset { get; set; } = 0;
		public int AnimationInterval { get; set; } = 100;

		public List<string> DialogueKey { get; set; } = new List<string> ();
		public bool UseSpeechBubble { get; set; } = false;

		public List<int> Emote { get; set; } = new List<int> ();

		public int Jump { get; set; } = 0;
		public int Shake { get; set; } = 0;

		public List<string> SoundCue { get; set; } = new List<string> ();

#pragma warning restore IDE1006

		protected void cloneInherited (Reaction clone)
		{
			base.cloneInherited (clone);

			clone.Animation = new List<int> (Animation);
			clone.AnimationDirectionOffset = AnimationDirectionOffset;
			clone.AnimationInterval = AnimationInterval;

			clone.DialogueKey = new List<string> (DialogueKey);
			clone.UseSpeechBubble = UseSpeechBubble;

			clone.Emote = new List<int> (Emote);

			clone.Jump = Jump;
			clone.Shake = Shake;

			clone.SoundCue = new List<string> (SoundCue);
		}

		public void performReaction (Farmer reagent = null, int facingRange = 4,
			object[] dialogueSubstitutions = null)
		{
			int dialogueDelay = 25;

			if (Animation.Count > 0)
			{
				int direction = Utilities.SpriteDirection ((reagent != null)
					? npc.getGeneralDirectionTowards (reagent.getStandingPosition ())
					: npc.FacingDirection);
				int offset = AnimationDirectionOffset * direction;
				Utilities.SetAnimation (npc.Sprite,
					Animation.Select ((frame) => frame + offset),
					AnimationInterval);
				dialogueDelay = Math.Max (dialogueDelay,
					AnimationInterval * Animation.Count);
			}

			if (Emote.Count > 0)
			{
				npc.doEmote (Emote.Random (Game1.random));
				dialogueDelay = Math.Max (dialogueDelay, 800);
			}

			if (Jump > 0)
			{
				npc.jump (Jump);
				dialogueDelay = Math.Max (dialogueDelay, 400);
			}

			if (Shake > 0)
			{
				npc.shake (Shake);
				dialogueDelay = Math.Max (dialogueDelay, Shake + 100);
			}

			if (SoundCue.Count > 0)
			{
				npc.currentLocation.localSound (SoundCue.Random (Game1.random));
				dialogueDelay = Math.Max (dialogueDelay, 200);
			}

			int facingPeriod = dialogueDelay + 3000;
			if (DialogueKey.Count > 0)
			{
				if (!UseSpeechBubble && reagent != null)
				{
					DelayedAction.functionAfterDelay (() =>
						reagent.CanMove = false, Shake + 1);
				}

				DelayedAction.functionAfterDelay (() =>
				{
					string dialogue = Game1.content.LoadString
						(DialogueKey.Random (Game1.random),
						dialogueSubstitutions ?? new object[] { reagent?.displayName });

					if (UseSpeechBubble)
					{
						npc.showTextAboveHead (dialogue);
					}
					else
					{
						Game1.drawDialogue (npc, dialogue);
						if (reagent != null)
							reagent.CanMove = true;
					}
				}, dialogueDelay);

				if (UseSpeechBubble)
					facingPeriod += 3000;
			}

			if (reagent != null)
			{
				npc.faceTowardFarmerForPeriod (facingPeriod, facingRange,
					faceAway: false, reagent);
			}
		}
	}
}
