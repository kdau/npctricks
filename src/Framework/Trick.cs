using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using StardewModdingAPI;
using StardewValley;
using System;

namespace NPCTricks
{
	public abstract class Trick : ICloneable
	{
		internal protected static IModHelper Helper => ModEntry.Instance.Helper;
		internal protected static IMonitor Monitor => ModEntry.Instance.Monitor;
		internal protected static ModConfig Config => ModConfig.Instance;

		[JsonIgnore]
		public Guid id { get; private set; }

		[JsonIgnore]
		public virtual bool oneTime => false;

		protected Trickster parent { get; private set; }
		protected NPC npc => parent?.npc;

		protected Trick ()
		{
			id = Guid.NewGuid ();
		}

		public abstract object Clone ();

		protected void cloneInherited (Trick clone)
		{
			clone.id = id;
		}

		internal virtual void prepare (Trickster parent)
		{
			this.parent = parent;
		}

		public virtual void apply (NPC npc)
		{ }

		public virtual void revert (NPC npc)
		{ }

		internal virtual void update (GameTime time)
		{ }
	}
}
