using StardewModdingAPI;
using System.Collections.Generic;

namespace NPCTricks
{
	public class TrickContext
	{
		internal protected static IModHelper Helper => ModEntry.Instance.Helper;
		internal protected static IMonitor Monitor => ModEntry.Instance.Monitor;
		internal protected static ModConfig Config => ModConfig.Instance;

#pragma warning disable IDE1006

		// Visual tricks

		public Sprite Sprite { get; set; } = null;
		public Breathing Breathing { get; set; } = null;
		public LightSource LightSource { get; set; } = null;
		public Assets Assets { get; set; } = null;
		public HiddenEmote HiddenEmote { get; set; } = null;
		// TODO: skin color light vs. dark for children

		// Spatial tricks

		public Spawn Spawn { get; set; } = null;
		// TODO: despawn
		public Movement Movement { get; set; } = null;
		public Warp Warp { get; set; } = null;
		// TODO: reassign schedule - accept dictionary as well as string to allow CP itemized patching
		// TODO: option to fix warp detection to act like standard size
		// TODO: miscellaneous pathfinding magic as needed/possible

		// Environmental reactions

		public DumpsterDive DumpsterDive { get; set; } = null;
		public NPCGreeting NPCGreeting { get; set; } = null;
		public MonsterGreeting MonsterGreeting { get; set; } = null;
		public PlayerGreeting PlayerGreeting { get; set; } = null;

		// Player interactions

		public Cursors Cursors { get; set; } = null;
		public Unmarriageable Unmarriageable { get; set; } = null;
		public bool LimitedGiftingReset { get; set; } = false;
		public List<GiftTaste> LimitedGifting { get; set; } =
			new List<GiftTaste> ();

#pragma warning restore IDE1006

		internal void merge (TrickContext target)
		{
			if (Sprite != null)
				target.Sprite = (Sprite) Sprite.Clone ();
			if (Breathing != null)
				target.Breathing = (Breathing) Breathing.Clone ();
			if (LightSource != null)
				target.LightSource = (LightSource) LightSource.Clone ();
			if (Assets != null)
				target.Assets = (Assets) Assets.Clone ();
			if (HiddenEmote != null)
				target.HiddenEmote = (HiddenEmote) HiddenEmote.Clone ();

			if (Spawn != null)
				target.Spawn = (Spawn) Spawn.Clone ();
			if (Movement != null)
				target.Movement = (Movement) Movement.Clone ();
			if (Warp != null)
				target.Warp = (Warp) Warp.Clone ();

			if (DumpsterDive != null)
				target.DumpsterDive = (DumpsterDive) DumpsterDive.Clone ();
			if (NPCGreeting != null)
				target.NPCGreeting = (NPCGreeting) NPCGreeting.Clone ();
			if (MonsterGreeting != null)
				target.MonsterGreeting = (MonsterGreeting) MonsterGreeting.Clone ();
			if (PlayerGreeting != null)
				target.PlayerGreeting = (PlayerGreeting) PlayerGreeting.Clone ();

			if (Cursors != null)
				target.Cursors = (Cursors) Cursors.Clone ();
			if (Unmarriageable != null)
				target.Unmarriageable = (Unmarriageable) Unmarriageable.Clone ();
			if (LimitedGiftingReset)
				target.LimitedGifting.Clear ();
			foreach (GiftTaste taste in LimitedGifting)
				target.LimitedGifting.Add ((GiftTaste) taste.Clone ());
		}

		internal virtual void clear ()
		{
			Sprite = null;
			Breathing = null;
			LightSource = null;
			Assets = null;
			HiddenEmote = null;

			Spawn = null;
			Movement = null;
			Warp = null;

			DumpsterDive = null;
			NPCGreeting = null;
			MonsterGreeting = null;
			PlayerGreeting = null;

			Cursors = null;
			Unmarriageable = null;
			LimitedGiftingReset = false;
			LimitedGifting.Clear ();
		}

		public IEnumerable<Trick> tricks ()
		{
			if (Sprite != null)
				yield return Sprite;
			if (Breathing != null)
				yield return Breathing;
			if (LightSource != null)
				yield return LightSource;
			if (Assets != null)
				yield return Assets;
			if (HiddenEmote != null)
				yield return HiddenEmote;

			if (Spawn != null)
				yield return Spawn;
			if (Movement != null)
				yield return Movement;
			if (Warp != null)
				yield return Warp;

			if (DumpsterDive != null)
				yield return DumpsterDive;
			if (NPCGreeting != null)
				yield return NPCGreeting;
			if (MonsterGreeting != null)
				yield return MonsterGreeting;
			if (PlayerGreeting != null)
				yield return PlayerGreeting;

			if (Cursors != null)
				yield return Cursors;
			if (Unmarriageable != null)
				yield return Unmarriageable;
			foreach (GiftTaste taste in LimitedGifting)
				yield return taste;
		}
	}
}
