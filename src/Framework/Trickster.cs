using Microsoft.Xna.Framework;
using StardewModdingAPI;
using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NPCTricks
{
	public class Tricktionary : Dictionary<string, Trickster>
	{ }

	public class Trickster : TrickContext
	{
		public readonly string name;
		public NPC npc { get; private set; }

		private TrickContext staticContext = null;
		private readonly Dictionary<Additional, bool> dynamicAdditionals = new ();

		private readonly List<Guid> tricksAppliedOneTime = new ();
		private readonly List<string> greetedCharacters = new ();

		public Trickster (string name)
		{
			this.name = name;
		}

		internal void addDynamicAdditional (Additional additional)
		{
			if (!dynamicAdditionals.ContainsKey (additional))
				dynamicAdditionals[additional] = false;
		}

		public IEnumerable<Additional> activeDynamicAdditionals =>
			dynamicAdditionals.Where ((kvp) => kvp.Value).Select ((kvp) => kvp.Key);

		internal override void clear ()
		{
			base.clear ();
			staticContext = null;
			dynamicAdditionals.Clear ();
		}

		internal void prepare ()
		{
			if (staticContext == null)
			{
				staticContext = new TrickContext ();
				merge (staticContext);
			}

			// Internally, Sprite must exist for Breathing to work.
			if (Sprite == null && Breathing != null)
				Sprite = new Sprite ();
			// And if Sprite is present, Breathing must be too for the defaults.
			else if (Sprite != null && Breathing == null)
				Breathing = new Breathing ();

			foreach (Trick trick in tricks ())
				trick.prepare (this);
		}

		internal void bind ()
		{
			NPC newNPC = Game1.getCharacterFromName (name);
			if (npc == newNPC && npc != null)
				return;

			if (npc != null)
			{
				if (newNPC != null)
				{
					Monitor.Log ($"NPC {name} was replaced with another instance",
						Config.VerboseLogging ? LogLevel.Info : LogLevel.Debug);
				}
				else if (Context.IsMainPlayer)
				{
					Monitor.Log ($"NPC {name} disappeared from world",
						Config.VerboseLogging ? LogLevel.Warn : LogLevel.Debug);
				}
			}
			else
			{
				if (newNPC != null)
				{
					Monitor.Log ($"Found NPC {name}",
						Config.VerboseLogging ? LogLevel.Info : LogLevel.Debug);
				}
				else if (Context.IsMainPlayer)
				{
					Monitor.LogOnce ($"Could not find NPC {name}",
						Config.VerboseLogging ? LogLevel.Warn : LogLevel.Debug);
				}
			}

			npc = newNPC;
			if (npc != null)
				updateDynamic (force: true);
		}

		internal void unbind ()
		{
			if (npc != null)
			{
				revertTricksFrom (npc);
				npc = null;
			}
		}

		internal void update (GameTime time)
		{
			foreach (Trick trick in tricks ())
			{
				try
				{
					trick.update (time);
				}
				catch (Exception e)
				{
					Utilities.Log (trick.GetType ().Name,
						$"Failed to update: {e.Message}",
						LogLevel.Warn);
					Utilities.Log (null, e.StackTrace, LogLevel.Trace);
				}
			}
		}

		internal void updateDynamic (bool force = false)
		{
			if (npc == null)
				return;

			if (staticContext == null)
			{
				Utilities.Log ("DynamicConditions",
					$"NPC {name} could not update due to missing base context",
					LogLevel.Error, once: true);
				return;
			}

			uint changes = 0;
			foreach (Additional additional in dynamicAdditionals.Keys.ToList ())
			{
				bool status = additional.shouldApplyNow (npc);
				if (dynamicAdditionals[additional] != status)
				{
					++changes;
					dynamicAdditionals[additional] = status;
				}
			}

			if (!force)
			{
				if (changes == 0)
					return;
				Utilities.Log ("DynamicConditions",
					$"NPC {name} had {changes} change(s)");
			}

			revertTricksFrom (npc);
			base.clear (); // keep staticContext and dynamicAdditionals

			staticContext.merge (this);
			foreach (Additional additional in activeDynamicAdditionals)
				additional.merge (this);

			prepare ();
			applyTricksTo (npc, includeOneTime: true);
		}

		public void applyTricksTo (NPC npc)
		{
			applyTricksTo (npc, includeOneTime: false);
		}

		private void applyTricksTo (NPC npc, bool includeOneTime)
		{
			npc ??= this.npc;
			if (npc == null)
				throw new ArgumentNullException (nameof (npc));
			foreach (Trick trick in tricks ())
			{
				if (trick.oneTime)
				{
					if (!includeOneTime || tricksAppliedOneTime.Contains (trick.id))
						continue;
					tricksAppliedOneTime.Add (trick.id);
				}
				try
				{
					trick.apply (npc);
				}
				catch (Exception e)
				{
					Utilities.Log (trick.GetType ().Name,
						$"Failed to apply to NPC {name}: {e.Message}",
						LogLevel.Error);
					Utilities.Log (null, e.StackTrace, LogLevel.Trace);
				}
			}
		}

		public void revertTricksFrom (NPC npc)
		{
			npc ??= this.npc;
			if (npc == null)
				throw new ArgumentNullException (nameof (npc));
			foreach (Trick trick in tricks ())
			{
				try
				{
					trick.revert (npc);
				}
				catch (Exception e)
				{
					Utilities.Log (trick.GetType ().Name,
						$"Failed to revert from NPC {name}: {e.Message}",
						LogLevel.Error);
					Utilities.Log (null, e.StackTrace, LogLevel.Trace);
				}
			}
		}

		public bool tryGreetings ()
		{
			if (MonsterGreeting != null && MonsterGreeting.initiate ())
				return true;
			if (PlayerGreeting != null && PlayerGreeting.initiate (Game1.player))
				return true;
			if (NPCGreeting != null && NPCGreeting.initiate (includeMonsters:
					MonsterGreeting == null))
				return true;
			return false;
		}

		public bool hasGreeted (string characterName)
		{
			return greetedCharacters.Contains (characterName);
		}

		public void markGreeted (string characterName)
		{
			if (!greetedCharacters.Contains (characterName))
				greetedCharacters.Add (characterName);
		}

		public bool canAcceptGiftFrom (Farmer giver)
		{
			if (npc == null)
				return false;

			// Not if the trickster is in a group 10-heart event with the giver.
			if (npc.Dialogue != null &&
					giver.activeDialogueEvents.Keys.Any ((key) =>
						key.Contains ("dumped") &&
							npc.Dialogue.ContainsKey (key)))
				return false;

			// Not if the trickster and the giver are divorced.
			if (giver.friendshipData.ContainsKey (name) &&
					giver.friendshipData[name].IsDivorced ())
				return false;

			return LimitedGifting.Any ((taste) => taste.canAcceptFrom (giver));
		}

		public bool acceptGiftFrom (Farmer giver)
		{
			return npc != null &&
				LimitedGifting.Any ((taste) => taste.acceptFrom (giver));
		}
	}
}
