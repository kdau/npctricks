using Microsoft.Xna.Framework;
using StardewModdingAPI;
using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NPCTricks
{
	public enum FacingDirection
	{
		up = Game1.up,
		right = Game1.right,
		down = Game1.down,
		left = Game1.left,
	}

	public static class Utilities
	{
		private static IModHelper Helper => ModEntry.Instance.Helper;
		private static IMonitor Monitor => ModEntry.Instance.Monitor;
		private static ModConfig Config => ModConfig.Instance;

		// Accesses Game1.multiplayer.
		public static Multiplayer MP => Helper.Reflection.GetField<Multiplayer>
			(typeof (Game1), "multiplayer").GetValue ();

		// Gets a random item from an enumerable collection.
		public static T Random<T> (this IEnumerable<T> list, Random rng)
		{
			if ((list?.Count () ?? 0) > 0)
				return list.ElementAt (rng.Next (list.Count ()));
			else
				return default;
		}

		public static int SpriteDirection (int facingDirection) =>
			SpriteDirection ((FacingDirection) facingDirection);

		public static int SpriteDirection (FacingDirection facingDirection) =>
			facingDirection switch
			{
				FacingDirection.up => 2,
				FacingDirection.right => 1,
				FacingDirection.down => 0,
				FacingDirection.left => 3,
				_ => 0,
			};

		public static void Log (string module, string message,
			LogLevel level = LogLevel.Info, bool? verbose = null,
			bool once = false)
		{
			verbose ??= level switch
			{
				LogLevel.Info => true,
				LogLevel.Debug => true,
				LogLevel.Trace => true,
				_ => false,
			};
			if (verbose.Value && !Config.VerboseLogging)
				return;
			if (module != null)
				message = $"[{module}] {message}";
			if (once)
				Monitor.LogOnce (message, level);
			else
				Monitor.Log (message, level);
		}

		// Gets the trickster for the NPC with the given name.
		public static Trickster GetTrickster (string npcName)
		{
			Trickster trickster = null;
			ModEntry.Instance?.tricksters?.TryGetValue (npcName, out trickster);
			return trickster;
		}

		// Gets the trickster for the given NPC.
		public static Trickster GetTrickster (NPC npc)
		{
			return GetTrickster (npc?.Name);
		}

		// Gets the custom-sprited trickster occupying the given tile from among
		// the given list of candidate NPCs.
		public static Trickster GetSpritedTricksterAt (IEnumerable<NPC> npcs,
			Point tileLocation, bool trimY = false)
		{
			Rectangle tile = new (tileLocation.X * 64, tileLocation.Y * 64, 64, 64);
			foreach (NPC npc in npcs ?? new NPC[] { })
			{
				Trickster trickster = GetTrickster (npc);
				if (trickster?.Sprite != null &&
						trickster.Sprite.getTilesBoundingBox (trimY: trimY)
							.Intersects (tile))
					return trickster;
			}
			return null;
		}

		// Gets the custom-sprited trickster occupying the given tile in the
		// given location.
		public static Trickster GetSpritedTricksterAt (GameLocation location,
			Point tileLocation, bool trimY = false)
		{
			return GetSpritedTricksterAt (location?.characters, tileLocation, trimY);
		}

		// Gets the custom-sprited trickster occupying the given tile during the
		// given event.
		public static Trickster GetSpritedTricksterAt (Event @event,
			Point tileLocation, bool trimY = false)
		{
			return GetSpritedTricksterAt (@event?.actors, tileLocation, trimY);
		}

		// Finds another NPC near the given NPC within the given radius,
		// matching the given filter if any.
		public delegate bool NPCFilter (NPC npc);
		public static NPC FindNearbyNPC (NPC npc, int tileRadius,
			NPCFilter filter = null)
		{
			Sprite mySprite = GetTrickster (npc)?.Sprite;

			foreach (NPC other in npc.currentLocation.characters)
			{
				// The NPC itself doesn't count.
				if (npc.Name.Equals (other.Name))
					continue;

				// The other NPC must match the filter, if any.
				if (filter != null && !filter (other))
					continue;

				// Check whether within the tile radius, using one or both
				// custom sprite dimensions if they exist.
				Sprite otherSprite = Utilities.GetTrickster (other)?.Sprite;
				bool withinRange =
					(mySprite != null)
						? ((otherSprite != null)
							? mySprite.withinRangeOfTileRectangle
								(otherSprite.getTilesBoundingBox (true, true),
									tileRadius)
							: mySprite.withinRangeOfTile
								(other.getTileLocationPoint (), tileRadius))
						: ((otherSprite != null)
							? otherSprite.withinRangeOfTile
								(npc.getTileLocationPoint (), tileRadius)
							: Vector2.Distance (npc.getTileLocation (),
								other.getTileLocation ()) <= tileRadius);

				if (withinRange)
					return other;
			}

			return null;
		}

		public static void SetAnimation (AnimatedSprite sprite,
			IEnumerable<int> frames, int interval, bool loop = false)
		{
			var animation = frames.Select ((frame) =>
				new FarmerSprite.AnimationFrame (frame, interval, 0, false, false)
			).ToList ();
			sprite.setCurrentAnimation (animation);
			sprite.loop = loop;
		}

		public static bool CheckForCharacterInteractionSafely (Vector2 tileLocation)
		{
			int oldCursor = Game1.mouseCursor;
			float oldTransparency = Game1.mouseCursorTransparency;
			Utility.checkForCharacterInteractionAtTile (tileLocation,
				Game1.player);
			bool result = Game1.mouseCursor != 0;
			Game1.mouseCursor = oldCursor;
			Game1.mouseCursorTransparency = oldTransparency;
			return result;
		}
	}
}
