using ExpandedPreconditionsUtility;
using Harmony;
using StardewModdingAPI;
using StardewModdingAPI.Utilities;
using System;
using System.Collections.Generic;

namespace NPCTricks
{
	public partial class ModEntry : Mod
	{
		internal static ModEntry Instance { get; private set; }

		internal HarmonyInstance harmony { get; private set; }
		internal JsonAssets.IApi jsonAssets { get; private set; }
		internal IConditionsChecker conditionsChecker { get; private set; }

		internal protected static ModConfig Config => ModConfig.Instance;

		private readonly List<ContentPack> contentPacks = new ();

		private readonly PerScreen<Tricktionary> tricksters_ = new (() => new ());
		public Tricktionary tricksters => tricksters_.Value;

		public override void Entry (IModHelper helper)
		{
			Instance = this;
			ModConfig.Load ();
			addEventHandlers ();
			addConsoleCommands ();

			// Apply Harmony patches.
			harmony = HarmonyInstance.Create (ModManifest.UniqueID);
			Game1Patches.Apply ();
			GameLocationPatches.Apply ();
			NPCPatches.Apply ();
			ProfileMenuPatches.Apply ();
			UtilityPatches.Apply ();
		}

		private void loadContentPacks ()
		{
			Utilities.Log (null, "Loading content packs...", LogLevel.Info,
				verbose: false);
			contentPacks.Clear ();
			foreach (IContentPack metadata in Helper.ContentPacks.GetOwned ())
			{
				try
				{
					contentPacks.Add (new ContentPack (metadata));
				}
				catch (Exception e)
				{
					Utilities.Log (null,
						$"    Skipping {metadata.Manifest.Name} due to load error: {e.Message}",
						LogLevel.Warn);
					Utilities.Log (null, e.StackTrace, LogLevel.Trace);
				}
			}
		}

		private void buildTricks ()
		{
			foreach (ContentPack pack in contentPacks)
				pack.merge (tricksters);
			foreach (Trickster trickster in tricksters.Values)
			{
				trickster.prepare ();
				trickster.bind ();
			}
		}

		private void rebuildTricks ()
		{
			foreach (Trickster trickster in tricksters.Values)
			{
				trickster.unbind ();
				trickster.clear ();
			}
			buildTricks ();
		}

		private void destroyTricks ()
		{
			foreach (Trickster trickster in tricksters.Values)
				trickster.unbind ();
			tricksters.Clear ();
		}

		private void bindTricksters ()
		{
			foreach (Trickster trickster in tricksters.Values)
				trickster.bind ();
		}
	}
}
