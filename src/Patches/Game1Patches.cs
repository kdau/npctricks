using Harmony;
using Microsoft.Xna.Framework;
using StardewModdingAPI;
using StardewValley;
using System;

namespace NPCTricks
{
	internal static class Game1Patches
	{
		private static IModHelper Helper => ModEntry.Instance.Helper;
		private static IMonitor Monitor => ModEntry.Instance.Monitor;
		private static ModConfig Config => ModConfig.Instance;

		private static HarmonyInstance Harmony => ModEntry.Instance.harmony;

		public static void Apply ()
		{
			Harmony.Patch (
				original: AccessTools.Method (typeof (Game1),
					nameof (Game1.warpCharacter), new Type[] { typeof (NPC),
					typeof (GameLocation), typeof (Vector2) }),
				postfix: new HarmonyMethod (typeof (Game1Patches),
					nameof (Game1Patches.warpCharacter_Postfix))
			);
		}

#pragma warning disable IDE1006

		public static void warpCharacter_Postfix (NPC character)
		{
			try
			{
				// Restore a custom speed after any method that resets it to 2.
				Trickster trickster = Utilities.GetTrickster (character);
				if (trickster?.Movement != null && character.Speed == 2)
					character.Speed = trickster.Movement.Speed;
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (warpCharacter_Postfix)}:\n{e}",
					LogLevel.Error);
				Utilities.Log (null, e.StackTrace, LogLevel.Trace);
			}
		}
	}
}
