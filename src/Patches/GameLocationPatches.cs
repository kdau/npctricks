using Harmony;
using Microsoft.Xna.Framework;
using StardewModdingAPI;
using StardewValley;
using System;
using System.Collections.Generic;

namespace NPCTricks
{
	internal static class GameLocationPatches
	{
		private static IModHelper Helper => ModEntry.Instance.Helper;
		private static IMonitor Monitor => ModEntry.Instance.Monitor;
		private static ModConfig Config => ModConfig.Instance;

		private static HarmonyInstance Harmony => ModEntry.Instance.harmony;

		public static void Apply ()
		{
			Harmony.Patch (
				original: AccessTools.Method (typeof (GameLocation),
					nameof (GameLocation.isCharacterAtTile)),
				postfix: new HarmonyMethod (typeof (GameLocationPatches),
					nameof (GameLocationPatches.isCharacterAtTile_Postfix))
			);

			Harmony.Patch (
				original: AccessTools.Method (typeof (GameLocation),
					nameof (GameLocation.isCollidingPosition), new Type[]
					{ typeof (Rectangle), typeof (xTile.Dimensions.Rectangle),
					typeof (bool), typeof (int), typeof (bool), typeof (Character),
					typeof (bool), typeof (bool), typeof (bool) }),
				prefix: new HarmonyMethod (typeof (GameLocationPatches),
					nameof (GameLocationPatches.isCollidingPosition_Prefix))
			);

			Harmony.Patch (
				original: AccessTools.Method (typeof (GameLocation),
					nameof (GameLocation.checkAction)),
				prefix: new HarmonyMethod (typeof (GameLocationPatches),
					nameof (GameLocationPatches.checkAction_Prefix))
			);
		}

#pragma warning disable IDE1006

		public static void isCharacterAtTile_Postfix (GameLocation __instance,
			ref NPC __result, Vector2 tileLocation)
		{
			try
			{
				// Consider a trickster on any tile which it occupies, subject
				// to the vertical trim to adjust for base game logic.
				if (__result != null)
					return;
				IEnumerable<NPC> npcs = (__instance.currentEvent != null)
					? __instance.currentEvent.actors
					: (IEnumerable<NPC>) __instance.characters;
				Trickster trickster = Utilities.GetSpritedTricksterAt (npcs,
					Utility.Vector2ToPoint (tileLocation), trimY: true);
				if (trickster != null)
					__result = trickster.npc;
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (isCharacterAtTile_Postfix)}:\n{e}",
					LogLevel.Error);
				Utilities.Log (null, e.StackTrace, LogLevel.Trace);
			}
		}

		public static bool isCollidingPosition_Prefix (Character character,
			ref bool glider)
		{
			try
			{
				// Treat non-colliding tricksters like flying monsters.
				if (character is NPC npc)
				{
					Trickster trickster = Utilities.GetTrickster (npc);
					if (trickster?.Movement != null && !trickster.Movement.Collision)
						glider = true;
				}
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (isCollidingPosition_Prefix)}:\n{e}",
					LogLevel.Error);
				Utilities.Log (null, e.StackTrace, LogLevel.Trace);
			}
			return true;
		}

		public static bool checkAction_Prefix (GameLocation __instance,
			ref bool __result, xTile.Dimensions.Location tileLocation,
			Farmer who)
		{
			try
			{
				// Consider a trickster on any tile which it occupies.
				Trickster trickster = Utilities.GetSpritedTricksterAt (__instance,
					new Point (tileLocation.X, tileLocation.Y));
				if (trickster?.npc != null &&
					trickster.npc.checkAction (who, __instance))
				{
					__result = true;
					return false;
				}
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (checkAction_Prefix)}:\n{e}",
					LogLevel.Error);
				Utilities.Log (null, e.StackTrace, LogLevel.Trace);
			}
			return true;
		}
	}
}
