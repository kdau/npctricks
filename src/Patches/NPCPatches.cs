using Harmony;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using StardewModdingAPI;
using StardewValley;
using StardewValley.Monsters;
using System;

namespace NPCTricks
{
	internal static class NPCPatches
	{
		private static IModHelper Helper => ModEntry.Instance.Helper;
		private static IMonitor Monitor => ModEntry.Instance.Monitor;
		private static ModConfig Config => ModConfig.Instance;

		private static HarmonyInstance Harmony => ModEntry.Instance.harmony;

		public static void Apply ()
		{
			Harmony.Patch (
				original: AccessTools.Method (typeof (NPC),
					nameof (NPC.Halt)),
				postfix: new HarmonyMethod (typeof (NPCPatches),
					nameof (NPCPatches.restoreSpeed))
			);
			Harmony.Patch (
				original: AccessTools.Method (typeof (NPC),
					"routeEndAnimationFinished"),
				postfix: new HarmonyMethod (typeof (NPCPatches),
					nameof (NPCPatches.restoreSpeed))
			);
			Harmony.Patch (
				original: AccessTools.Method (typeof (NPC),
					nameof (NPC.changeSchedulePathDirection)),
				postfix: new HarmonyMethod (typeof (NPCPatches),
					nameof (NPCPatches.restoreSpeed))
			);
			Harmony.Patch (
				original: AccessTools.Method (typeof (NPC),
					nameof (NPC.randomSquareMovement)),
				postfix: new HarmonyMethod (typeof (NPCPatches),
					nameof (NPCPatches.restoreSpeed))
			);

			Harmony.Patch (
				original: AccessTools.Method (typeof (NPC),
					nameof (NPC.checkAction)),
				prefix: new HarmonyMethod (typeof (NPCPatches),
					nameof (NPCPatches.checkAction_Prefix))
			);

			Harmony.Patch (
				original: AccessTools.Method (typeof (NPC),
					nameof (NPC.GetBoundingBox)),
				postfix: new HarmonyMethod (typeof (NPCPatches),
					nameof (NPCPatches.GetBoundingBox_Postfix))
			);

			Harmony.Patch (
				original: AccessTools.Method (typeof (NPC),
					nameof (NPC.getStandingX)),
				postfix: new HarmonyMethod (typeof (NPCPatches),
					nameof (NPCPatches.getStandingX_Postfix))
			);

			Harmony.Patch (
				original: AccessTools.Method (typeof (NPC), nameof (NPC.draw),
					new Type[] { typeof (SpriteBatch), typeof (float) }),
				prefix: new HarmonyMethod (typeof (NPCPatches),
					nameof (NPCPatches.draw_Prefix))
			);

			Harmony.Patch (
				original: AccessTools.Method (typeof (NPC),
					nameof (NPC.drawAboveAlwaysFrontLayer)),
				prefix: new HarmonyMethod (typeof (NPCPatches),
					nameof (NPCPatches.drawAboveAlwaysFrontLayer_Prefix))
			);

			Harmony.Patch (
				original: AccessTools.Method (typeof (NPC),
					nameof (NPC.sayHiTo)),
				prefix: new HarmonyMethod (typeof (NPCPatches),
					nameof (NPCPatches.sayHiTo_Prefix))
			);

			Harmony.Patch (
				original: AccessTools.Method (typeof (NPC),
					"engagementResponse"),
				prefix: new HarmonyMethod (typeof (NPCPatches),
					nameof (NPCPatches.engagementResponse_Prefix))
			);
		}

#pragma warning disable IDE1006

		public static void restoreSpeed (NPC __instance)
		{
			try
			{
				// Restore a custom speed after any method that resets it to 2.
				Trickster trickster = Utilities.GetTrickster (__instance);
				if (trickster?.Movement != null && __instance.Speed == 2)
					__instance.Speed = trickster.Movement.Speed;
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (restoreSpeed)}:\n{e}",
					LogLevel.Error);
				Utilities.Log (null, e.StackTrace, LogLevel.Trace);
			}
		}

		public static bool checkAction_Prefix (NPC __instance, ref bool __result,
			Farmer who)
		{
			try
			{
				Trickster trickster = Utilities.GetTrickster (__instance);
				if (trickster == null)
					return true;

				// The NPC and farmer must be ready for a gift transaction.
				if (__instance.IsInvisible || __instance.isSleeping.Value ||
						!who.CanMove || who.isRidingHorse ())
					return true;

				// The active item must be giftable.
				if (!(who.ActiveObject?.canBeGivenAsGift () ?? false))
					return true;

				// Attempt a limited gifting transaction.
				if (trickster.acceptGiftFrom (who))
				{
					__result = true;
					return false;
				}
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (checkAction_Prefix)}:\n{e}",
					LogLevel.Error);
				Utilities.Log (null, e.StackTrace, LogLevel.Trace);
			}
			return true;
		}

		public static void GetBoundingBox_Postfix (NPC __instance,
			ref Rectangle __result)
		{
			try
			{
				Trickster trickster = Utilities.GetTrickster (__instance);
				if (trickster?.Sprite != null)
					__result = trickster.Sprite.getBoundingBox ();
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (GetBoundingBox_Postfix)}:\n{e}",
					LogLevel.Error);
				Utilities.Log (null, e.StackTrace, LogLevel.Trace);
			}
		}

		public static void getStandingX_Postfix (NPC __instance,
			ref int __result)
		{
			try
			{
				Trickster trickster = Utilities.GetTrickster (__instance);
				if (trickster?.Sprite != null)
					__result = trickster.Sprite.getStandingX ();
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (getStandingX_Postfix)}:\n{e}",
					LogLevel.Error);
				Utilities.Log (null, e.StackTrace, LogLevel.Trace);
			}
		}

		public static bool draw_Prefix (NPC __instance, SpriteBatch b,
			float alpha)
		{
			try
			{
				Trickster trickster = Utilities.GetTrickster (__instance);
				if (trickster?.Sprite != null)
				{
					trickster.Sprite.draw (b, alpha);
					return false;
				}
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (draw_Prefix)}:\n{e}",
					LogLevel.Error);
				Utilities.Log (null, e.StackTrace, LogLevel.Trace);
			}
			return true;
		}

		public static bool drawAboveAlwaysFrontLayer_Prefix (NPC __instance,
			SpriteBatch b)
		{
			try
			{
				Trickster trickster = Utilities.GetTrickster (__instance);
				if (trickster?.Sprite != null)
				{
					trickster.Sprite.drawAboveAlwaysFrontLayer (b);
					return false;
				}
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (drawAboveAlwaysFrontLayer_Prefix)}:\n{e}",
					LogLevel.Error);
				Utilities.Log (null, e.StackTrace, LogLevel.Trace);
			}
			return true;
		}

		public static bool sayHiTo_Prefix (NPC __instance, Character c)
		{
			try
			{
				// If the initiator is a trickster with custom NPC greeting
				// logic, bail out because this will be handled separately.
				// Same applies to monster greeting logic for monster targets.
				Trickster initiator = Utilities.GetTrickster (__instance);
				if (initiator?.NPCGreeting != null ||
						(initiator?.MonsterGreeting != null && c is Monster m))
					return false;

				// Let the original method handle cases that would fail.
				string hi = __instance.getHi (c.displayName);
				if (c is not NPC npc || hi == null)
					return true;

				// If the recipient is a trickster with custom NPC greeting logic,
				// replicate the base game logic for the initiator but use the
				// custom logic to reciprocate.
				Trickster recipient = Utilities.GetTrickster (npc);
				if (__instance is Monster && recipient?.MonsterGreeting != null)
				{
					__instance.showTextAboveHead (hi);
					recipient.MonsterGreeting.initiateUnconditionally (__instance);
					return false;
				}
				if (recipient?.NPCGreeting != null)
				{
					if (recipient.NPCGreeting.Receive)
					{
						__instance.showTextAboveHead (hi);
						recipient.NPCGreeting.reciprocate (__instance);
					}
					else
					{
						Utilities.Log (nameof (NPCGreeting),
							$"Stopping {__instance.Name} from greeting {c.Name} because {c.Name} does not receive greetings");
					}
					return false;
				}
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (sayHiTo_Prefix)}:\n{e}",
					LogLevel.Error);
				Utilities.Log (null, e.StackTrace, LogLevel.Trace);
			}
			return true;
		}

		public static bool engagementResponse_Prefix (NPC __instance, Farmer who)
		{
			try
			{
				// Reject the engagement if the trickster is unmarriageable.
				Trickster trickster = Utilities.GetTrickster (__instance);
				if (trickster?.Unmarriageable != null)
				{
					trickster.Unmarriageable.reject (who);
					return false;
				}
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (engagementResponse_Prefix)}:\n{e}",
					LogLevel.Error);
				Utilities.Log (null, e.StackTrace, LogLevel.Trace);
			}
			return true;
		}
	}
}
