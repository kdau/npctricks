using Harmony;
using Microsoft.Xna.Framework;
using StardewModdingAPI;
using StardewValley;
using StardewValley.Menus;
using System;

namespace NPCTricks
{
	internal static class ProfileMenuPatches
	{
		private static IModHelper Helper => ModEntry.Instance.Helper;
		private static IMonitor Monitor => ModEntry.Instance.Monitor;
		private static ModConfig Config => ModConfig.Instance;

		private static HarmonyInstance Harmony => ModEntry.Instance.harmony;

		public static void Apply ()
		{
			Harmony.Patch (
				original: AccessTools.Method (typeof (ProfileMenu),
					nameof (ProfileMenu.update)),
				postfix: new HarmonyMethod (typeof (ProfileMenuPatches),
					nameof (ProfileMenuPatches.update_Postfix))
			);
		}

#pragma warning disable IDE1006

		public static void update_Postfix (ProfileMenu __instance, GameTime time)
		{
			try
			{
				AnimatedSprite sprite =
					Helper.Reflection.GetField<AnimatedSprite> (__instance,
						"_animatedSprite").GetValue ();
				if (sprite == null)
					return;

				if (Helper.Reflection.GetField<Vector2> (__instance,
						"_characterEntrancePosition").GetValue () != Vector2.Zero)
					return;

				if (Helper.Reflection.GetField<float> (__instance,
						"_hiddenEmoteTimer").GetValue () <= 0f)
					return;

				Trickster trickster =
					Utilities.GetTrickster (__instance.GetCharacter () as NPC);
				if (trickster?.HiddenEmote != null)
					trickster.HiddenEmote.updateInMenu (sprite, time);
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (update_Postfix)}:\n{e}",
					LogLevel.Error);
				Utilities.Log (null, e.StackTrace, LogLevel.Trace);
			}
		}
	}
}
