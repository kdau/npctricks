using Harmony;
using Microsoft.Xna.Framework;
using StardewModdingAPI;
using StardewValley;
using StardewValley.Locations;
using System;

namespace NPCTricks
{
	internal static class UtilityPatches
	{
		private static IModHelper Helper => ModEntry.Instance.Helper;
		private static IMonitor Monitor => ModEntry.Instance.Monitor;
		private static ModConfig Config => ModConfig.Instance;

		private static HarmonyInstance Harmony => ModEntry.Instance.harmony;

		public static void Apply ()
		{
			Harmony.Patch (
				original: AccessTools.Method (typeof (Utility),
					nameof (Utility.tileWithinRadiusOfPlayer)),
				postfix: new HarmonyMethod (typeof (UtilityPatches),
					nameof (UtilityPatches.tileWithinRadiusOfPlayer_Postfix))
			);

			Harmony.Patch (
				original: AccessTools.Method (typeof (Utility),
					nameof (Utility.isThereAFarmerOrCharacterWithinDistance)),
				prefix: new HarmonyMethod (typeof (UtilityPatches),
					nameof (UtilityPatches.isThereAFarmerOrCharacterWithinDistance_Prefix))
			);

			Harmony.Patch (
				original: AccessTools.Method (typeof (Utility),
					nameof (Utility.checkForCharacterInteractionAtTile)),
				postfix: new HarmonyMethod (typeof (UtilityPatches),
					nameof (UtilityPatches.checkForCharacterInteractionAtTile_Postfix))
			);
		}

#pragma warning disable IDE1006

		public static void tileWithinRadiusOfPlayer_Postfix (ref bool __result,
			int xTile, int yTile, int tileRadius, Farmer f)
		{
			try
			{
				if (__result == true || f.currentLocation == null)
					return;
				Trickster trickster = Utilities.GetSpritedTricksterAt
					(f.currentLocation, new Point (xTile, yTile));
				if (trickster != null)
				{
					__result = trickster.Sprite.withinRangeOfTile
						(f.getTileLocationPoint (), tileRadius, false);
				}
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (tileWithinRadiusOfPlayer_Postfix)}:\n{e}",
					LogLevel.Error);
				Utilities.Log (null, e.StackTrace, LogLevel.Trace);
			}
		}

		public static bool isThereAFarmerOrCharacterWithinDistance_Prefix (ref
			Character __result, Vector2 tileLocation, int tilesAway,
			GameLocation environment)
		{
			try
			{
				// Detect the invocation used for dumpster dives.
				bool isDumpsterDive = tilesAway == 7 && environment?.Name == "Town";

				// Check for a character, handling sprited tricksters correctly.
				Point point = Utility.Vector2ToPoint (tileLocation);
				foreach (NPC character in environment.characters)
				{
					Trickster trickster = Utilities.GetTrickster (character);
					int radius = (isDumpsterDive
						? trickster?.DumpsterDive?.Range : null) ?? tilesAway;
					if ((trickster?.Sprite != null)
						? trickster.Sprite.withinRangeOfTile (point, radius)
						: Vector2.Distance (character.getTileLocation (),
							tileLocation) <= radius)
					{
						// If this is a dumpster dive and we found a trickster
						// with custom logic for that, run it and return null
						// to the caller so it won't do anything with the NPC.
						if (isDumpsterDive && trickster?.DumpsterDive != null)
						{
							trickster.DumpsterDive.observe (Game1.player);
							__result = null;
							return false;
						}

						// Otherwise, this is the result for the caller.
						__result = character;
						return false;
					}
				}

				// Fall through to checking for a farmer.
				__result = Utility.isThereAFarmerWithinDistance (tileLocation,
					tilesAway, environment);
				return false;
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (isThereAFarmerOrCharacterWithinDistance_Prefix)}:\n{e}",
					LogLevel.Error);
				Utilities.Log (null, e.StackTrace, LogLevel.Trace);
			}
			return true;
		}

		public static void checkForCharacterInteractionAtTile_Postfix (Vector2 tileLocation,
			Farmer who)
		{
			try
			{
				// Only perform cleanup for visible NPCs.
				NPC npc = Game1.currentLocation.isCharacterAtTile (tileLocation);
				if (npc == null || npc.IsInvisible)
					return;

				// Only perform cleanup for tricksters.
				Trickster trickster = Utilities.GetTrickster (npc);
				if (trickster == null)
					return;

				if (who.ActiveObject?.canBeGivenAsGift () ?? false)
				{
					// Add the gift cursor for limited gifting.
					if (!(Game1.currentLocation is MovieTheater) &&
						!Game1.eventUp && trickster.canAcceptGiftFrom (who))
					{
						Game1.mouseCursor = 3;
						Game1.mouseCursorTransparency = Utility.tileWithinRadiusOfPlayer
							((int) tileLocation.X, (int) tileLocation.Y, 1, who)
								? 1f : 0.5f;
					}
					// If the trickster has no gift tastes but the active item is
					// giftable, remove the dialogue cursor since it will fail.
					else if (Game1.mouseCursor == 4 &&
						!Game1.NPCGiftTastes.ContainsKey (npc.Name))
					{
						Game1.mouseCursor = 0;
					}
				}

				// Apply any custom cursors that have been configured.
				if (trickster.Cursors != null)
					Game1.mouseCursor = trickster.Cursors.mapCursor (Game1.mouseCursor);
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (checkForCharacterInteractionAtTile_Postfix)}:\n{e}",
					LogLevel.Error);
				Utilities.Log (null, e.StackTrace, LogLevel.Trace);
			}
		}
	}
}
