using Microsoft.Xna.Framework.Graphics;
using StardewValley;

namespace NPCTricks
{
	public class Assets : Trick
	{
#pragma warning disable IDE1006

		public string Sprites { get; set; }
		public string Portraits { get; set; }

#pragma warning restore IDE1006

		public override object Clone ()
		{
			Assets clone = new ();
			cloneInherited (clone);
			clone.Sprites = Sprites;
			clone.Portraits = Portraits;
			return clone;
		}

		public override void apply (NPC npc)
		{
			if (Sprites != null)
			{
				Utilities.Log ("Assets",
					$"Switching {parent.name} to spritesheet {Sprites}");
				npc.Sprite.LoadTexture (Sprites);
			}

			if (Portraits != null)
			{
				Utilities.Log ("Assets",
					$"Switching {parent.name} to portrait sheet {Portraits}");
				npc.Portrait = Game1.content.Load<Texture2D> (Portraits);
			}
		}

		public override void revert (NPC npc)
		{
			if (Sprites != null)
			{
				Utilities.Log ("Assets",
					$"Reverting {parent.name} to default spritesheet");
				npc.Sprite.LoadTexture ($"Characters\\{npc.Name}");
			}

			if (Portraits != null)
			{
				Utilities.Log ("Assets",
					$"Reverting {parent.name} to default portrait sheet");
				npc.Portrait = Game1.content.Load<Texture2D> ($"Portraits\\{npc.Name}");
			}
		}
	}
}
