using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using StardewValley;
using System;
using System.Collections.Generic;

namespace NPCTricks
{
	public class Breathing : Trick
	{
#pragma warning disable IDE1006

		public float Scale { get; set; } = 1f;
		public int Rate { get; set; } = 16;
		public List<Rectangle> Areas { get; set; } =
			new List<Rectangle> ();

#pragma warning restore IDE1006

		public override object Clone ()
		{
			Breathing clone = new ();
			cloneInherited (clone);
			clone.Scale = Scale;
			clone.Rate = Rate;
			clone.Areas = new List<Rectangle> (Areas);
			return clone;
		}

		internal override void prepare (Trickster parent)
		{
			base.prepare (parent);

			// Expand the one-, two- and three-value shorthands.
			if (Areas.Count == 1)
				Areas.Add (Areas[0]);
			if (Areas.Count == 2)
				Areas.Add (Areas[0]);
			if (Areas.Count == 3)
			{
				// When calculating left rect from right rect, flip the X axis.
				Rectangle leftRect = Areas[1];
				int width = parent.Sprite?.Width ?? 16;
				leftRect.X = width - leftRect.X - leftRect.Width;
				Areas.Add (leftRect);
			}
		}

		public void draw (SpriteBatch b, float alpha, Vector2 localPosition,
			Rectangle box)
		{
			if (Scale == 0f)
				return;

			int Width = npc.Sprite.SpriteWidth;
			int Height = npc.Sprite.SpriteHeight;

			Rectangle sourceRect;
			Vector2 offset;
			if (Areas.Count >= 4)
			{
				int direction = Utilities.SpriteDirection (npc.FacingDirection);
				sourceRect = Areas[direction];
				sourceRect.X += npc.Sprite.SourceRect.X;
				sourceRect.Y += npc.Sprite.sourceRect.Y;
				offset = 4f * new Vector2
					(Areas[direction].X + sourceRect.Width / 2f
						- Width / 2 + Width * 4 / 2 / 4,
					Areas[direction].Y + sourceRect.Height / 2f
						- Height * 3f / 4f + box.Height / 2 / 4f);
			}
			else
			{
				sourceRect = npc.Sprite.SourceRect;
				sourceRect.Y += Height / 2 + Height / 32;
				sourceRect.Height = Height / 4;
				sourceRect.X += Width / 4;
				sourceRect.Width = Width / 2;
				offset = new Vector2 (Width * 4 / 2, 8f);
				if (npc.Age == 2)
				{
					sourceRect.Y += Height / 6 + 1;
					sourceRect.Height /= 2;
					offset.Y += Height / 8 * 4;
				}
				else if (npc.Gender == 1)
				{
					++sourceRect.Y;
					offset.Y -= 4f;
					sourceRect.Height /= 2;
				}
			}

			double phase = Game1.currentGameTime.TotalGameTime.TotalMilliseconds
				/ (60000.0 / (Rate * 2.0 * Math.PI));
			float scaleAdd = Scale * Math.Max (0f,
				(float) Math.Ceiling (Math.Sin (phase +
					npc.DefaultPosition.X * 20.0)) / 4f);

			b.Draw (npc.Sprite.Texture, localPosition + offset,
				sourceRect,
				Color.White * alpha,
				npc.rotation,
				new Vector2 (sourceRect.Width / 2, sourceRect.Height / 2 + 1),
				Math.Max (0.2f, npc.Scale) * 4f + scaleAdd,
				npc.flip ? SpriteEffects.FlipHorizontally : SpriteEffects.None,
				Math.Max (0f, npc.drawOnTop ? 0.992f
					: npc.getStandingY () / 10000f + 0.001f));
		}
	}
}
