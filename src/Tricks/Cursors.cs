using StardewModdingAPI;
using System;

namespace NPCTricks
{
	public enum StockCursor
	{
		Normal = 0,
		Wait = 1,
		Pet = 2,
		Gift = 3,
		Talk = 4,
		Inspect = 5,
		Energy = 6,
		Health = 7,
		Finger = 44,
		ControllerA = 45,
		ControllerX = 46,
		ControllerB = 47,
		ControllerY = 48,
	};

	public class Cursors : Trick
	{
#pragma warning disable IDE1006

		public object Gift { get; set; } = StockCursor.Gift;
		public object Talk { get; set; } = StockCursor.Talk;

#pragma warning restore IDE1006

		private int giftCursor = (int) StockCursor.Gift;
		private int talkCursor = (int) StockCursor.Talk;

		public override object Clone ()
		{
			Cursors clone = new ();
			cloneInherited (clone);
			clone.Gift = Gift;
			clone.Talk = Talk;
			return clone;
		}

		internal override void prepare (Trickster parent)
		{
			base.prepare (parent);
			giftCursor = ResolveCursor (Gift, (int) StockCursor.Gift);
			talkCursor = ResolveCursor (Talk, (int) StockCursor.Talk);
		}

		public int mapCursor (int baseCursor)
		{
			return baseCursor switch
			{
				3 => giftCursor,
				4 => talkCursor,
				_ => baseCursor,
			};
		}

		private static int ResolveCursor (object reference, int fallback)
		{
			if (reference.GetType () == typeof (long))
				return (int) (long) reference;

			if (reference.GetType () == typeof (StockCursor))
				return (int) (StockCursor) reference;

			if (reference.GetType () != typeof (string))
			{
				Utilities.Log ("Cursors", $"Skipping unrecognized type '{reference}'",
					LogLevel.Warn);
				return fallback;
			}
			string name = (string) reference;

			if (Enum.TryParse (name, true, out StockCursor stock))
				return (int) stock;

			Utilities.Log ("Cursors", $"Skipping unknown cursor '{name}'",
				LogLevel.Info, verbose: false);
			return fallback;
		}
	}
}
