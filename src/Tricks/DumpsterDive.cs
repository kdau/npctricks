using StardewValley;

namespace NPCTricks
{
	public class DumpsterDive : EnvironmentalReaction
	{
#pragma warning disable IDE1006

		public int FriendshipPoints { get; set; } = -25;

#pragma warning restore IDE1006

		public DumpsterDive ()
		{
			Emote.Add (12);
			DialogueKey.Add ("Data\\ExtraDialogue:Town_DumpsterDiveComment_Adult");
			Range = 7;
		}

		public override object Clone ()
		{
			DumpsterDive clone = new ();
			cloneInherited (clone);
			clone.FriendshipPoints = FriendshipPoints;
			return clone;
		}

		public bool observe (Farmer diver)
		{
			if (!shouldInitiate ())
			{
				Utilities.Log (nameof (DumpsterDive),
					$"{parent.name} ignoring {diver.Name}");
				return false;
			}

			Utilities.Log (nameof (DumpsterDive),
				$"{parent.name} reacting to {diver.Name}");

			Utilities.MP.globalChatInfoMessage ("TrashCan", diver.Name, parent.name);

			if (FriendshipPoints != 0 && diver.friendshipData.ContainsKey (parent.name))
				diver.changeFriendship (FriendshipPoints, npc);

			performReaction (diver);
			return true;
		}
	}
}
