using StardewModdingAPI;
using StardewValley;
using System.Collections.Generic;
using SObject = StardewValley.Object;

namespace NPCTricks
{
	public class GiftTaste : Reaction
	{
		private static JsonAssets.IApi JsonAssets =>
			ModEntry.Instance.jsonAssets;

#pragma warning disable IDE1006

		public List<object> Items { get; set; } = new List<object> ();

		public int FriendshipPoints { get; set; } = 0;
		public float QualityMultiplier { get; set; } = 0.125f;
		public float BirthdayMultiplier { get; set; } = 8f;
		public float SpouseMultiplier { get; set; } = 0.5f;

#pragma warning restore IDE1006

		private readonly List<int> items = new ();

		public override object Clone ()
		{
			GiftTaste clone = new ();
			cloneInherited (clone);
			clone.Items = new List<object> (Items);
			clone.FriendshipPoints = FriendshipPoints;
			clone.QualityMultiplier = QualityMultiplier;
			clone.BirthdayMultiplier = BirthdayMultiplier;
			clone.SpouseMultiplier = SpouseMultiplier;
			return clone;
		}

		internal override void prepare (Trickster parent)
		{
			base.prepare (parent);
			items.Clear ();
			foreach (object reference in Items)
			{
				int item = ResolveObjectReference (reference);
				if (item != -1)
					items.Add (item);
			}
		}

		public bool canAcceptFrom (Farmer giver)
		{
			SObject gift = giver.ActiveObject;
			if (gift.GetType () != typeof (SObject) || gift.bigCraftable.Value)
				return false;
			foreach (int item in items)
			{
				if (item < 0 && gift.Category == item)
					return true;
				if (item >= 0 && gift.ParentSheetIndex == item)
					return true;
			}
			return false;
		}

		public bool acceptFrom (Farmer giver)
		{
			SObject gift = giver.ActiveObject;
			if (!canAcceptFrom (giver))
			{
				Utilities.Log ("LimitedGifting",
					$"{parent.name} rejecting {gift?.name ?? "nothing"} from {giver.Name}");
				return false;
			}

			Utilities.Log ("LimitedGifting",
				$"{parent.name} accepting {gift.name} from {giver.Name}");

			// Stop the giver and have the face the trickster.
			giver.Halt ();
			giver.faceGeneralDirection (npc.getStandingPosition ());
			((FarmerSprite) giver.Sprite).animateBackwardsOnce
				(64 + 8 * Utilities.SpriteDirection (giver.FacingDirection), 50f);
			giver.completelyStopAnimatingOrDoingAction ();

			// Remove the gift from the giver's inventory with needful updates.
			giver.onGiftGiven (npc, gift);
			giver.reduceActiveItemByOne ();
			if (SoundCue == null)
				giver.currentLocation.localSound ("give_gift");

			// Update friendship if applicable.
			if (FriendshipPoints != 0 &&
				giver.friendshipData.ContainsKey (parent.name))
			{
				float multiplier = 1f;
				if (FriendshipPoints > 0)
					multiplier += gift.Quality * QualityMultiplier;
				if (npc.isBirthday (Game1.Date.Season, Game1.Date.DayOfMonth))
					multiplier *= BirthdayMultiplier;
				if (giver.Equals (npc.getSpouse ()))
					multiplier *= SpouseMultiplier;
				giver.changeFriendship ((int) (FriendshipPoints * multiplier), npc);
				giver.completeQuest (25); // How To Win Friends
			}

			// React to the gift.
			performReaction (giver);
			return true;
		}

		private static int ResolveObjectReference (object reference)
		{
			if (reference.GetType () == typeof (long))
				return (int) (long) reference;

			if (reference.GetType () != typeof (string))
			{
				Utilities.Log ("LimitedGifting",
					$"Skipping gift taste item with unrecognized type '{reference}'",
					LogLevel.Warn);
				return -1;
			}
			string name = (string) reference;

			int custom = JsonAssets?.GetObjectId (name) ?? -1;
			if (custom != -1)
				return custom;

			foreach (var pair in Game1.objectInformation)
			{
				if (pair.Value.Split ('/')[0] == name)
					return pair.Key;
			}

			Utilities.Log ("LimitedGifting",
				$"Skipping unknown gift taste item '{name}'",
				LogLevel.Info, verbose: false);
			return -1;
		}
	}
}
