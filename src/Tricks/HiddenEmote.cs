using Microsoft.Xna.Framework;
using StardewModdingAPI.Utilities;
using StardewValley;
using System;
using System.Collections.Generic;

namespace NPCTricks
{
	public class HiddenEmote : Trick
	{
#pragma warning disable IDE1006

		public List<int> Frames { get; set; } = new List<int> ();
		public int Interval { get; set; } = 200;
		public int WorldClicks { get; set; } = 4;
		public int WorldDuration { get; set; } = 3000;

		private AnimatedSprite menuProxy;
		private double lastMenuTime;

		private AnimatedSprite worldProxy;
		private int worldTimer;
		private int worldLastFrame;

#pragma warning restore IDE1006

		private static readonly PerScreen<string> ClickedNPC = new (() => null);
		private static readonly PerScreen<int> ClickCount = new (() => 0);

		public override object Clone ()
		{
			HiddenEmote clone = new ();
			cloneInherited (clone);
			clone.Frames = new List<int> (Frames);
			clone.Interval = Interval;
			clone.WorldClicks = WorldClicks;
			clone.WorldDuration = WorldDuration;
			return clone;
		}

		public void updateInMenu (AnimatedSprite sprite, GameTime time)
		{
			if (menuProxy == null ||
				time.ElapsedGameTime.TotalMilliseconds > lastMenuTime + 100.0)
			{
				Utilities.Log ("HiddenEmote",
					$"Playing in menu for {parent.name}");
				menuProxy = sprite.Clone ();
				Utilities.SetAnimation (menuProxy, Frames, Interval, loop: true);
			}

			menuProxy.animateOnce (time);
			lastMenuTime = time.ElapsedGameTime.TotalMilliseconds;
			sprite.CurrentFrame = menuProxy.CurrentFrame;
			sprite.UpdateSourceRect ();
		}

		public void playInWorld ()
		{
			Utilities.Log ("HiddenEmote",
				$"Playing in world for {parent.name}");
			worldProxy = npc.Sprite.Clone ();
			worldTimer = WorldDuration;
			worldLastFrame = npc.Sprite.CurrentFrame;
			Utilities.SetAnimation (worldProxy, Frames, Interval, loop: true);
		}

		internal override void update (GameTime time)
		{
			if (worldTimer > 0)
				worldTimer -= time.ElapsedGameTime.Milliseconds;
			if (npc?.Sprite != null && worldProxy != null)
			{
				if (worldTimer > 0)
				{
					worldProxy.animateOnce (time);
					npc.Sprite.CurrentFrame = worldProxy.CurrentFrame;
				}
				else
				{
					worldProxy = null;
					npc.Sprite.CurrentFrame = worldLastFrame;
				}
				npc.Sprite.UpdateSourceRect ();
			}
		}

		internal static void HandleWorldAction (Vector2 tileLocation)
		{
			// Find a visible NPC on the pointed tile.
			NPC npc = Game1.currentLocation.isCharacterAtTile (tileLocation);
			if (npc == null || npc.IsInvisible)
			{
				ClickedNPC.Value = null;
				ClickCount.Value = 0;
				return;
			}

			// Track how many times the current NPC has been clicked.
			if (ClickedNPC.Value == npc.Name)
			{
				++ClickCount.Value;
			}
			else
			{
				ClickedNPC.Value = npc.Name;
				ClickCount.Value = 1;
			}

			// Check whether the NPC, if any, is a trickster with a
			// world-clickable hidden emote.
			Trickster trickster = Utilities.GetTrickster (npc);
			if (trickster?.HiddenEmote == null ||
					trickster.HiddenEmote.WorldClicks <= 0)
				return;

			// Play the hidden emote after the correct number of clicks.
			if (ClickCount.Value >= trickster.HiddenEmote.WorldClicks)
			{
				ClickCount.Value = 0;
				trickster.HiddenEmote.playInWorld ();
			}
		}
	}
}
