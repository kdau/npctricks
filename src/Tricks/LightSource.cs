using Microsoft.Xna.Framework;
using StardewModdingAPI;
using StardewValley;
using System.Collections.Generic;
using SLightSource = StardewValley.LightSource;

namespace NPCTricks
{
	public class LightSource : Trick
	{
		public enum TextureType
		{
			none = 0,
			lantern = 1,
			window = 2,
			sconce = 4,
			cauldron = 5,
			indoorWindow = 6,
			projector = 7,
		}

#pragma warning disable IDE1006

		public TextureType Texture { get; set; } = TextureType.lantern;
		public Point Offset { get; set; } = Point.Zero;
		public float Radius { get; set; } = 1f;
		public Color Color { get; set; } = Color.White;

#pragma warning restore IDE1006

		private readonly Dictionary<NPC, int> identifiers = new ();

		public override object Clone ()
		{
			LightSource clone = new ();
			cloneInherited (clone);
			clone.Texture = Texture;
			clone.Offset = Offset;
			clone.Radius = Radius;
			clone.Color = Color;
			return clone;
		}

		public override void apply (NPC npc)
		{
			if (identifiers.ContainsKey (npc))
			{
				Utilities.Log ("LightSource",
					$"Light source already present on {parent.name}",
					LogLevel.Warn);
				return;
			}

			if (Texture == TextureType.none)
				return;

			Utilities.Log ("LightSource", $"Adding to {parent.name}");
			identifiers.Add (npc, Game1.random.Next (-99999, 99999));
			addOrUpdateSource (npc);
		}

		public override void revert (NPC npc)
		{
			if (!identifiers.ContainsKey (npc))
				return;

			Utilities.Log ("LightSource", $"Removing from {parent.name}");
			Utility.removeLightSource (identifiers[npc]);
			identifiers.Remove (npc);
		}

		internal override void update (GameTime time)
		{
			foreach (NPC npc in identifiers.Keys)
				addOrUpdateSource (npc);
		}

		private void addOrUpdateSource (NPC npc)
		{
			if (npc.currentLocation != Game1.currentLocation)
			{
				Utility.removeLightSource (identifiers[npc]);
				return;
			}

			Vector2 position =
				Utility.PointToVector2 (Sprite.GetTilesBoundingBox (npc).Center) +
				4 * Utility.PointToVector2 (Offset);

			SLightSource existingSource =
				Utility.getLightSource (identifiers[npc]);
			if (existingSource != null)
			{
				existingSource.position.Value = position;
				return;
			}

			Game1.currentLightSources.Add (new SLightSource ((int) Texture,
				position, Radius, Utility.getOppositeColor (Color),
				identifiers[npc], SLightSource.LightContext.None, 0L));
		}
	}
}
