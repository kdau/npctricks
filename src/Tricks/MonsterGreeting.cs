using StardewValley;
using StardewValley.Locations;
using StardewValley.Monsters;

namespace NPCTricks
{
	public class MonsterGreeting : EnvironmentalReaction
	{
		public MonsterGreeting ()
		{
			Range = 8;
			Jump = 8;
			Shake = 250;
			UseSpeechBubble = true;
		}

		public override object Clone ()
		{
			MonsterGreeting clone = new ();
			cloneInherited (clone);
			return clone;
		}

		public bool initiate ()
		{
			if (!shouldInitiate () || npc.currentLocation.farmers.Count == 0)
				return false;

			// Spouses in the farmhouse have a built-in monster reaction.
			if (npc.isMarried () && npc.currentLocation is FarmHouse)
				return false;

			// Check for nearby monsters based on true sprite size.
			NPC recipient = Utilities.FindNearbyNPC (npc, Range,
				(candidate) => candidate is Monster);
			if (recipient == null)
				return false;

			// Must not have greeted this monster before.
			if (parent.hasGreeted (recipient.Name))
				return false;

			initiateUnconditionally (recipient);
			return true;
		}

		public void initiateUnconditionally (NPC recipient)
		{
			Utilities.Log (nameof (MonsterGreeting),
				$"{parent.name} greeting {recipient.Name}");
			parent.markGreeted (recipient.Name);
			performReaction (dialogueSubstitutions:
				new object[] { recipient.displayName });
		}
	}
}
