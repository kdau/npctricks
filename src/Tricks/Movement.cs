using StardewValley;

namespace NPCTricks
{
	public class Movement : Trick
	{
#pragma warning disable IDE1006

		public int Speed { get; set; } = 2;
		public bool Collision { get; set; } = true;

#pragma warning restore IDE1006

		public override object Clone ()
		{
			Movement clone = new ();
			cloneInherited (clone);
			clone.Speed = Speed;
			clone.Collision = Collision;
			return clone;
		}

		public override void apply (NPC npc)
		{
			npc.Speed = Speed;
			npc.collidesWithOtherCharacters.Value = Collision;
			npc.farmerPassesThrough = !Collision;
		}

		public override void revert (NPC npc)
		{
			npc.Speed = 2;
			npc.collidesWithOtherCharacters.Value = true;
			npc.farmerPassesThrough = false;
		}
	}
}
