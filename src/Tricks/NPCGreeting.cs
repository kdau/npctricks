using StardewValley;
using StardewValley.Characters;
using StardewValley.Locations;
using StardewValley.Monsters;

namespace NPCTricks
{
	public class NPCGreeting : EnvironmentalReaction
	{
#pragma warning disable IDE1006

		public bool Receive { get; set; } = true;
		public double Reciprocate { get; set; } = 0.66;

#pragma warning restore IDE1006

		public NPCGreeting ()
		{
			Initiate = 0.3;
			Indoors = false;
			Standing = false;
			Night = false;
			DialogueKey.Add ("Strings\\StringsFromCSFiles:NPC.cs.4060");
			UseSpeechBubble = true;
		}

		public override object Clone ()
		{
			NPCGreeting clone = new ();
			cloneInherited (clone);
			clone.Receive = Receive;
			clone.Reciprocate = Reciprocate;
			return clone;
		}

		public bool initiate (bool includeMonsters = true)
		{
			if (!shouldInitiate () || npc.currentLocation.farmers.Count == 0)
				return false;

			// Spouses don't greet normal villager NPCs. They also have a
			// special reaction to monsters in the farmhouse.
			bool includeVillagers = true;
			if (npc.isMarried ())
			{
				if (!includeMonsters || npc.currentLocation is FarmHouse)
					return false;
				includeVillagers = false;
			}

			// Check for nearby NPCs based on true sprite size.
			NPC recipient = Utilities.FindNearbyNPC (npc, Range,
				(candidate) => !(candidate is Horse) &&
					(includeVillagers || candidate is Monster) &&
					(includeMonsters || !(candidate is Monster)));
			if (recipient == null)
				return false;

			// If another trickster with custom logic, must receive greetings.
			Trickster recipientTrickster = Utilities.GetTrickster (recipient);
			if (recipientTrickster?.NPCGreeting != null &&
					!recipientTrickster.NPCGreeting.Receive)
			{
				Utilities.Log (nameof (NPCGreeting),
					$"{parent.name} not greeting {recipient.Name} because {recipient.Name} doesn't receive greetings");
				return false;
			}

			// Must not have greeted this NPC before.
			if (parent.hasGreeted (recipient.Name))
				return false;

			initiateUnconditionally (recipient, recipientTrickster);
			return true;
		}

		public void initiateUnconditionally (NPC recipient,
			Trickster recipientTrickster)
		{
			Utilities.Log (nameof (NPCGreeting),
				$"{parent.name} greeting {recipient.Name}");

			// Mark the recipient as greeted.
			parent.markGreeted (recipient.Name);

			// Perform the reaction itself.
			performReaction (dialogueSubstitutions:
				new object[] { recipient.displayName });

			// If another trickster, hand reciprocation off to it.
			if (recipientTrickster?.NPCGreeting != null)
			{
				recipientTrickster.NPCGreeting.reciprocate (npc);
			}
			// Replicate the base game reciprocation logic.
			else if (Game1.random.NextDouble () < 0.66 &&
				recipient.getHi (npc.displayName) != null)
			{
				recipient.showTextAboveHead (recipient.getHi (npc.displayName),
					-1, 2, 3000, 1000 + Game1.random.Next (500));
			}
		}

		public bool reciprocate (NPC initiator)
		{
			if (!(Game1.random.NextDouble () < Reciprocate))
			{
				Utilities.Log (nameof (NPCGreeting),
					$"{parent.name} ignoring greeting from {initiator.Name}");
				return false;
			}

			Utilities.Log (nameof (NPCGreeting),
				$"{parent.name} reciprocating greeting from {initiator.Name}");

			DelayedAction.functionAfterDelay (() =>
			{
				performReaction (dialogueSubstitutions:
					new object[] { initiator.displayName });
			}, 1000 + Game1.random.Next (500));

			return true;
		}
	}
}
