using Microsoft.Xna.Framework;
using StardewValley;
using System.Collections.Generic;

namespace NPCTricks
{
	public class PlayerGreeting : EnvironmentalReaction
	{
		public PlayerGreeting ()
		{
			UseSpeechBubble = true;
		}

		public override object Clone ()
		{
			PlayerGreeting clone = new ();
			cloneInherited (clone);
			return clone;
		}

		public bool initiate (Farmer player)
		{
			if (!shouldInitiate () ||
					!npc.currentLocation.Equals (player.currentLocation))
				return false;

			// Check for range based on true sprite size.
			bool inRange = (parent.Sprite != null)
				? parent.Sprite.withinRangeOfTile
					(player.getTileLocationPoint (), Range)
				: Vector2.Distance (npc.getTileLocation (),
					player.getTileLocation ()) > Range;
			if (!inRange)
				return false;

			if (parent.hasGreeted (player.Name))
				return false;
			parent.markGreeted (player.Name);

			Utilities.Log (nameof (PlayerGreeting),
				$"{parent.name} greeting {player.Name}");
			performReaction (player, Range);
			return true;
		}
	}
}
