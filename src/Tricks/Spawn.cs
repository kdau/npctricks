using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using StardewValley;
using System;

namespace NPCTricks
{
	public class Spawn : Trick
	{
		[JsonIgnore]
		public override bool oneTime => true;

#pragma warning disable IDE1006

		public string Location { get; set; }
		public Point Position { get; set; }
		public FacingDirection FacingDirection { get; set; } = FacingDirection.up;

#pragma warning restore IDE1006

		public override object Clone ()
		{
			Spawn clone = new ();
			cloneInherited (clone);
			clone.Location = Location;
			clone.Position = Position;
			clone.FacingDirection = FacingDirection;
			return clone;
		}

		internal override void prepare (Trickster parent)
		{
			base.prepare (parent);
			if (Position.Equals (Point.Zero))
			{
				int x = 0;
				int y = 0;
				Utility.getDefaultWarpLocation (Location, ref x, ref y);
				Position = new Point (x, y);
			}
		}

		public override void apply (NPC npc)
		{
			if (npc != null)
				throw new ArgumentOutOfRangeException (nameof (npc));

			Utilities.Log ("Spawn",
				$"Spawning {parent.name} in {Location} at {Position}");

			GameLocation location = Game1.getLocationFromName (Location);
			Vector2 position = Utility.PointToVector2 (Position) * 64f;

			AnimatedSprite sprite = new (
				parent.Assets?.Sprites ?? $"Characters\\{parent.name}", 0,
				parent.Sprite?.Width ?? 16, parent.Sprite?.Height ?? 32);
			Texture2D portraits = Game1.content.Load<Texture2D> (
				parent.Assets?.Portraits ?? $"Portraits\\{parent.name}");

			npc = new NPC (sprite, position, Location, (int) FacingDirection,
				parent.name, null, portraits, false, null);
			location.addCharacter (npc);
		}
	}
}
