using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using StardewValley;
using StardewValley.BellsAndWhistles;
using System;

namespace NPCTricks
{
	public class Sprite : Trick
	{
#pragma warning disable IDE1006

		public int Width { get; set; } = 16;
		// TODO: Height > 32 (and confirm 16 works)
		public int Height { get; set; } = 32;

		public float Opacity { get; set; } = 1f;
		public bool Shadow { get; set; } = true;

#pragma warning restore IDE1006

		public override object Clone ()
		{
			Sprite clone = new ();
			cloneInherited (clone);
			clone.Width = Width;
			clone.Height = Height;
			clone.Opacity = Opacity;
			clone.Shadow = Shadow;
			return clone;
		}

		public override void apply (NPC npc)
		{
			npc.Sprite.SpriteWidth = Width;
			npc.Sprite.SpriteHeight = Height;
			npc.Sprite.UpdateSourceRect ();

			npc.HideShadow = !Shadow;
		}

		public override void revert (NPC npc)
		{
			npc.Sprite.SpriteWidth = 16;
			npc.Sprite.SpriteHeight = 32;
			npc.Sprite.UpdateSourceRect ();

			npc.HideShadow = false;
		}

		public Rectangle getBoundingBox ()
		{
			if (npc == null)
				return Rectangle.Empty;
			return new Rectangle ((int) npc.Position.X + 8,
				(int) npc.Position.Y + 16, Width * 4 - 16, 32);
		}

		public int getStandingX ()
		{
			if (npc == null)
				return 0;
			return (int) npc.Position.X + 32;
		}

		public static Rectangle GetTilesBoundingBox (NPC npc,
			bool trimX = false, bool trimY = false)
		{
			if (npc == null)
				return Rectangle.Empty;
			int Width = npc.Sprite.SpriteWidth;
			int Height = npc.Sprite.SpriteHeight;
			return new Rectangle ((int) npc.Position.X,
				(int) npc.Position.Y - 4 * (Height - (trimY ? 32 : 16)),
				4 * (Width - (trimX ? 16 : 0)), 4 * (Height - (trimY ? 16 : 0)));
		}

		public Rectangle getTilesBoundingBox (bool trimX = false,
			bool trimY = false)
		{
			return GetTilesBoundingBox (npc, trimX, trimY);
		}

		public bool withinRangeOfPoint (Point point, int radius,
			bool geometric = true)
		{
			// Adjust the tiles bounding box to account for the base game
			// behavior of doing point-to-point comparison.
			Rectangle box = getTilesBoundingBox (true, true);

			// Find the minimum distance in both axes.
			int xDist = Math.Min (Math.Abs (box.Left - point.X),
				Math.Abs (box.Right - point.X));
			int yDist = Math.Min (Math.Abs (box.Top - point.Y),
				Math.Abs (box.Bottom - point.Y));

			// Check the distance by Pythagorean theorem or in each axis.
			return geometric
				? Math.Sqrt (Math.Pow (xDist, 2) + Math.Pow (yDist, 2)) <= radius
				: xDist <= radius && yDist <= radius;
		}

		public bool withinRangeOfTile (Point tileLocation, int tileRadius,
			bool geometric = true)
		{
			return withinRangeOfPoint (new Point (tileLocation.X * 64,
				tileLocation.Y * 64), tileRadius * 64, geometric);
		}

		public bool withinRangeOfTileRectangle (Rectangle box, int tileRadius,
			bool geometric = true)
		{
			// This isn't quite right, but it gets the job done.
			return
				withinRangeOfPoint (new Point (box.Left, box.Top), tileRadius, geometric) ||
				withinRangeOfPoint (new Point (box.Left, box.Bottom), tileRadius, geometric) ||
				withinRangeOfPoint (new Point (box.Right, box.Top), tileRadius, geometric) ||
				withinRangeOfPoint (new Point (box.Right, box.Bottom), tileRadius, geometric);
		}

		public Vector2 getOverHeadPosition ()
		{
			if (npc == null)
				return Vector2.Zero;
			Rectangle box = getTilesBoundingBox ();
			float x = npc.FacingDirection switch
			{
				Game1.right => box.Right - 32,
				Game1.left => box.Left + 32,
				_ => box.Center.X,
			};
			return Game1.GlobalToLocal (new Vector2 (x,
				box.Top - 96 + npc.yJumpOffset));
		}

		public static Vector2 ShakeVector () =>
			new (Game1.random.Next (-1, 2), Game1.random.Next (-1, 2));

		public void draw (SpriteBatch b, float alpha)
		{
			if (npc?.Sprite == null || npc.IsInvisible ||
					!Utility.isOnScreen (npc.Position, 128))
				return;

			Vector2 localPosition = npc.getLocalPosition (Game1.viewport);
			int shakeTimer = Helper.Reflection.GetField<int> (npc, "shakeTimer")
				.GetValue ();
			Rectangle box = npc.GetBoundingBox ();

			if (npc.swimming.Value)
			{
				Rectangle swimRect = npc.Sprite.SourceRect;
				swimRect.Height /= 2;
				swimRect.Height -= (int) (npc.yOffset / 4f);
				b.Draw (npc.Sprite.Texture, localPosition +
						new Vector2 (32f, 80 + npc.yJumpOffset * 2) +
						((shakeTimer > 0) ? ShakeVector () : Vector2.Zero) -
						new Vector2 (0f, npc.yOffset),
					swimRect,
					Color.White * Opacity,
					npc.rotation,
					new Vector2 (32f, 96f) / 4f,
					Math.Max (0.2f, npc.Scale) * 4f,
					npc.flip ? SpriteEffects.FlipHorizontally : SpriteEffects.None,
					Math.Max (0f, npc.drawOnTop ? 0.991f
						: npc.getStandingY () / 10000f));
				b.Draw (Game1.staminaRect, new Rectangle
						((int) localPosition.X + (int) npc.yOffset + 8,
						(int) localPosition.Y - 128 +
							npc.Sprite.SourceRect.Height * 4 + 48 +
							npc.yJumpOffset * 2 - (int) npc.yOffset,
						npc.Sprite.SourceRect.Width * 4 -
							(int) npc.yOffset * 2 - 16,
						4),
					Game1.staminaRect.Bounds,
					Color.White * 0.75f, 0f,
					Vector2.Zero,
					SpriteEffects.None,
					npc.getStandingY () / 10000f + 0.001f);
			}
			else
			{
				b.Draw (npc.Sprite.Texture, localPosition +
						new Vector2 (Width * 4 / 2, box.Height / 2) +
						((shakeTimer > 0) ? ShakeVector () : Vector2.Zero),
					npc.Sprite.SourceRect,
					Color.White * Opacity * alpha,
					npc.rotation,
					new Vector2 (Width / 2, Height * 3f / 4f),
					Math.Max (0.2f, npc.Scale) * 4f,
					(npc.flip || (npc.Sprite.CurrentAnimation != null &&
							npc.Sprite.CurrentAnimation[npc.Sprite.currentAnimationIndex].flip))
						? SpriteEffects.FlipHorizontally : SpriteEffects.None,
					Math.Max (0f, npc.drawOnTop ? 0.991f
						: npc.getStandingY () / 10000f));
			}

			if (parent.Breathing != null && shakeTimer <= 0 &&
					!npc.swimming.Value && npc.Sprite.currentFrame < 16)
				parent.Breathing.draw (b, Opacity * alpha, localPosition, box);

			if (npc.isGlowing)
			{
				b.Draw (npc.Sprite.Texture, localPosition +
						new Vector2 (Width * 4 / 2, box.Height / 2) +
						((shakeTimer > 0) ? ShakeVector () : Vector2.Zero),
					npc.Sprite.SourceRect,
					npc.glowingColor * npc.glowingTransparency * Opacity,
					npc.rotation,
					new Vector2 (Width / 2, Height * 3f / 4f),
					Math.Max (0.2f, npc.Scale) * 4f,
					npc.flip ? SpriteEffects.FlipHorizontally : SpriteEffects.None,
					Math.Max (0f, npc.drawOnTop ? 0.99f
						: npc.getStandingY () / 10000f + 0.001f));
			}

			if (npc.IsEmoting && !Game1.eventUp)
			{
				Vector2 position = getOverHeadPosition ();
				position += new Vector2 (-32f, 0f);
				b.Draw (Game1.emoteSpriteSheet, position, new Rectangle
						(npc.CurrentEmoteIndex * 16 % Game1.emoteSpriteSheet.Width,
						npc.CurrentEmoteIndex * 16 / Game1.emoteSpriteSheet.Width * 16,
						16, 16),
					Color.White, 0f, Vector2.Zero, 4f, SpriteEffects.None,
					npc.getStandingY () / 10000f);
			}
		}

		public void drawAboveAlwaysFrontLayer (SpriteBatch b)
		{
			if (npc == null)
				return;

			string textAboveHead = Helper.Reflection.GetField<string> (npc,
				"textAboveHead").GetValue ();
			int textAboveHeadTimer = Helper.Reflection.GetField<int> (npc,
				"textAboveHeadTimer").GetValue ();

			if (textAboveHeadTimer <= 0 || textAboveHead == null)
				return;

			float textAboveHeadAlpha = Helper.Reflection.GetField<float> (npc,
				"textAboveHeadAlpha").GetValue ();
			int textAboveHeadColor = Helper.Reflection.GetField<int> (npc,
				"textAboveHeadColor").GetValue ();
			int textAboveHeadStyle = Helper.Reflection.GetField<int> (npc,
				"textAboveHeadStyle").GetValue ();

			// Calculate the position for a potentially wide sprite.
			Vector2 position = getOverHeadPosition ();
			position += new Vector2 (-8f, 0f);
			if (textAboveHeadStyle == 0)
				position += ShakeVector ();

			// Draw the text with correct positioning.
			SpriteText.drawStringWithScrollCenteredAt (b, textAboveHead,
				(int) position.X, (int) position.Y, "", textAboveHeadAlpha,
				textAboveHeadColor, 1, npc.getTileY () * 64f / 10000f +
				0.001f + npc.getTileX () / 10000f);
		}

		public void drawBoundingBoxes (SpriteBatch b)
		{
			if (npc == null)
				return;

			DrawBox (b, Game1.GlobalToLocal (Game1.viewport,
				getTilesBoundingBox ()), Color.Blue);

			DrawBox (b, Game1.GlobalToLocal (Game1.viewport,
				getBoundingBox ()), Color.Red);

			// Draw reticle at nominal position.
			Point pos = Utility.Vector2ToPoint (npc.Position);
			b.Draw (Game1.staminaRect, Game1.GlobalToLocal (Game1.viewport,
				new Rectangle (pos.X - 4, pos.Y - 4, 8, 8)), Color.Green);
		}

		private static void DrawBox (SpriteBatch b, Rectangle box, Color color)
		{
			b.Draw (Game1.staminaRect, new Rectangle
				(box.X, box.Y, box.Width, 1), color);
			b.Draw (Game1.staminaRect, new Rectangle
				(box.X, box.Y, 1, box.Height), color);
			b.Draw (Game1.staminaRect, new Rectangle
				(box.X + box.Width, box.Y, 1, box.Height), color);
			b.Draw (Game1.staminaRect, new Rectangle
				(box.X, box.Y + box.Height, box.Width, 1), color);
		}
	}
}
