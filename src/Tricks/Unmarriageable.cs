using StardewValley;
using System.Collections.Generic;

namespace NPCTricks
{
	public class Unmarriageable : Reaction
	{
#pragma warning disable IDE1006

		public List<string> MusicTrack = new () { "none" };

#pragma warning restore IDE1006

		public override object Clone ()
		{
			Unmarriageable clone = new ();
			cloneInherited (clone);
			clone.MusicTrack = new List<string> (MusicTrack);
			return clone;
		}

		public void reject (Farmer proposer)
		{
			Utilities.Log (nameof (Unmarriageable),
				$"{parent.name} rejecting marriage proposal from {proposer.Name}");

			if (MusicTrack.Count > 0)
			{
				string track = MusicTrack.Random (Game1.random);
				if (track != "continue")
					Game1.changeMusicTrack (track);
			}

			npc.CurrentDialogue.Clear ();
			proposer.completelyStopAnimatingOrDoingAction ();

			performReaction (proposer);
		}
	}
}
