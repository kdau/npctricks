using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using StardewValley;

namespace NPCTricks
{
	public class Warp : Trick
	{
		[JsonIgnore]
		public override bool oneTime => true;

#pragma warning disable IDE1006

		public string Location { get; set; }
		public Point Position { get; set; }
		public FacingDirection FacingDirection { get; set; } = FacingDirection.up;
		public bool Halt { get; set; } = false;

#pragma warning restore IDE1006

		public override object Clone ()
		{
			Warp clone = new ();
			cloneInherited (clone);
			clone.Location = Location;
			clone.Position = Position;
			clone.FacingDirection = FacingDirection;
			clone.Halt = Halt;
			return clone;
		}

		internal override void prepare (Trickster parent)
		{
			base.prepare (parent);
			if (Position.Equals (Point.Zero))
			{
				int x = 0;
				int y = 0;
				Utility.getDefaultWarpLocation (Location, ref x, ref y);
				Position = new Point (x, y);
			}
		}

		public override void apply (NPC npc)
		{
			Utilities.Log ("Warp",
				$"Warping {parent.name} to {Location} at {Position}");
			Game1.warpCharacter (npc, Location, Position);
			npc.faceDirection ((int) FacingDirection);
			if (Halt)
				npc.Halt ();
		}
	}
}
